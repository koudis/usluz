<?php
/**
 * @author jan.kubalek
 *
 * Tato trida slouzi pro zavedeni Usluz.
 * pouziti:
 * 	include 'Usluz/Usluz.php';
 * 	Usluz::autoload();
*/

define('USLUZ_ACT_DIR', dirname(__FILE__). '/');



/**
 *
*/ 
class Usluz {
	static public function autoload($default_template = 'Def', array $global_conf = array()) {
		set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));
		spl_autoload_register(function($class_name) {
			$class_name = preg_replace('/\\\\/', '/', $class_name);
			if(stream_resolve_include_path($class_name . '.php')) {
				require_once $class_name . '.php';
				return true;
			}
		});

		static::init_global_conf($global_conf, $default_template);
		set_include_path(get_include_path() . PATH_SEPARATOR . 
			\Usluz\Core\GlobalConf::get('PROCESS_BASE_DIR'));

		Usluz\Core\Core::init();
		Usluz\Lib\Lib::init();
	}

	/**
	 * Nacte danou variaci Usluz klienta
	 * (Workflow sablony)
	 * @var $name string
	 * @var $args array
	 * @return \Usluz\Core\Workflow\AbstractWorkflow
	*/ 
	static public function &load($name, $args) {
		if(!class_exists($name))
			throw new Exception('problem, problem, problem');

		if($args)
			$_tmp = new $name(...$args);
		else $_tmp = new $name();

		\Usluz\Core\GlobalDir::create();
		\Usluz\Core\GlobalCron::process();

		return $_tmp;
	}

	/**
	 * Upravi globalni konfiguraci
	 * @param array $global_conf
	 * @return void
	*/ 
	static private function init_global_conf(array &$global_conf, &$default_template) {
		\Usluz\Core\GlobalConf::init($default_template);
		foreach($global_conf as $k => $v)
			\Usluz\Core\GlobalConf::set($k, $v);
	}
}
