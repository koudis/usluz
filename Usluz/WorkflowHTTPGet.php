<?php

require_once dirname(__FILE__) . '/autoload.php';
use \Usluz\Core\Error;



class WorkflowHTTPGet extends \Usluz\Core\Workflow\Workflow {
	public function __construct(\Usluz\Core\Other\Conf\Conf $ini = null) {
		parent::__construct($ini);
	}

	static public function handle_critic_error(\Exception &$e) {
		$_t = yaml_parse_file('conf/critic_error.yaml', true);
		$email = $_t['email'];
		mail($email['to'], $email['subject'] . 'HTTPGetRequest', print_r($e, true), 'From: ' . $email['from'] . "\r\n");
		echo "NASTALA KRITICKA CHYBA: kontaktuj spravce!";
	}

	static protected function &create_comm(\Usluz\Core\Other\Conf\Conf &$conf) {
		$_t = new \Usluz\Core\Comm\Com_HTTPGet($conf, $this->get_next_arg(); $this->get_next_arg());
		return $_t;
	}
}
