<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache
*/

namespace Usluz\Triggers\Abstr;



/**
 * Obalka, ktera umoznuje pravazat Request a Response Triggery
 * @see \Usluz\Core\Comm\Iface\InterfaceTriggerEnvelope
*/
abstract class AbstractEnvelope implements \Usluz\Core\Comm\Iface\InterfaceTriggerEnvelope {
	/**
	 * @var \Usluz\Triggers\Abstr\AbstractTrigger
	*/
	private $response_trigger;

	/**
	 * @var \Usluz\Triggers\Abstr\AbstractTrigger
	*/
	private $request_trigger;

	/**
	 * @var \Usluz\Core\Storge\Storage
	*/
	private $local_storage = NULL;



	public function __construct(\Usluz\Triggers\Abstr\AbstractTrigger $request = NULL, \Usluz\Triggers\Abstr\AbstractTrigger $response = NULL) {
		$this->request_trigger  = $request;
		$this->response_trigger = $response;

		if($this->request_trigger && $this->response_trigger) {
			$this->local_storage = new \Usluz\Core\Storage\Storage();
			$this->request_trigger->set_local_storage($this->local_storage);
			$this->response_trigger->set_local_storage($this->local_storage);
		}
	}



	/**
	 *
	 * @return \Usluz\Triggers\Abstr\AbstractTrigger
	*/
	public function get_request_trigger() {
		return $this->request_trigger;
	}

	/**
	 *
	 * @return \Usluz\Triggers\Abstr\AbstractTrigger
	*/
	public function get_response_trigger() {
		return $this->response_trigger;
	}
}
