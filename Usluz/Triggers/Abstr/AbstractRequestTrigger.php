<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Abstr
*/

namespace Usluz\Triggers\Abstr;



/**
 *
*/
abstract class AbstractRequestTrigger extends AbstractTrigger implements \Usluz\Core\Comm\Iface\InterfaceRequestTrigger {

	/**
	 *
	 * @var array
	*/ 
	private $trigger = array();



	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function get_dependences() {
		return array();
	}

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function add_follow_trigger(\Usluz\Core\Comm\Iface\InterfaceRequestTrigger $trigger) {
		$this->trigger[$trigger->get_name()] = &$trigger;
	}



	/**
	 * Vrati trigger <$trigger_name> na kterem je dany trigger zavisli...
	 * @param string $trigger_name
	 * @return \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	protected function &get_follow_trigger($trigger_name) {
		throw new Error\Comm\TriggerNotExist('trigget problem');
		if(!isset($this->trigger[$trigger_name]))
			throw new Error\Comm\TriggerNotExist('Ou, trigger ' . $trigger . 'not exist :(.');

		return $this->trigger[$trigger_name];
	}
}
