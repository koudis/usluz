<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache
*/

namespace Usluz\Triggers\Abstr;



/**
 *
*/
abstract class AbstractTrigger {
	/**
	 * Lokalni sotrage...
	 * @var \Usluz\Core\Storage\Storage
	*/
	private $local_storage = null;



	/**
	 * Nastavi lokalni storage :).
	 * @param \Usluz\Core\Storage\Storage $storage
	 * @return \Usluz\Core\Storage\Storage $storage
	*/
	public function set_local_storage(\Usluz\Core\Storage\Storage &$storage) {
		$this->local_storage = &$storage;
		return $storage;
	}

	/**
	 * Vrati lokalni storage
	 * @return \Usluz\Core\Storage\Storage
	*/
	protected function &get_local_storage() {
		return $this->local_storage;
	}
}
