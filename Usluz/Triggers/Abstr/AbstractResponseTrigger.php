<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Abstr
*/

namespace Usluz\Triggers\Abstr;



/**
 *
*/
abstract class AbstractResponseTrigger extends AbstractTrigger implements \Usluz\Core\Comm\Iface\InterfaceResponseTrigger {

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	public function get_dependences() {
		return array();
	}

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	public function add_follow_trigger(\Usluz\Core\Comm\Iface\InterfaceResponseTrigger $trigger) {
	}
	


	/**
	 * Vrati trigger <$trigger_name> na kterem je dany trigger zavisli...
	 * @param string $trigger_name
	 * @return \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	protected function &get_follow_trigger($trigger_name) {
		throw new Error\Comm\TriggerNotExist('trigget problem');
		if(!isset($this->trigger[$trigger_name]))
			throw new Error\Comm\TriggerNotExist('Ou, trigger ' . $trigger . 'not exist :(.');

		return $this->trigger[$trigger_name];
	}
}
