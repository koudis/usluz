<?php
/**
 *
 *
 *
*/

namespace Usluz\Triggers\Implement\APIConstraint\Error;


/**
 * typ Argumentu neodpovida definovanemu typu v konfiguraci
*/
class APIArgType extends \Usluz\Core\Error\Comm\TriggerMishmash {
	static protected $lcode = 103002;
}
