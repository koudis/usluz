<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\APIConstraint
*/

namespace Usluz\Triggers\Implement\APIConstraint;
use \Usluz\Triggers\Abstr;
use \Usluz\Core\Storage;
use \Usluz\Core\Other\Conf;
use \Usluz\Core\Comm\Trigger;



/**
 *
*/
class APIConstraintRequestTrigger extends Abstr\AbstractRequestTrigger {
	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function get_name() {
		return 'APIConstraint';
	}

	public function get_dependices() {
		return [
			'SimpleRouter',
		];
	}

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function execute(Conf\Conf $conf, Storage\Storage &$request, Storage\Storage $server) {
		$method    = $request->get_value('method_type');
		$submethod = $request->get_value('submethod_type');

		$values = $request->get_values();

		$pconf = NULL;
		try {
			$pconf = $conf->get_config($method, $submethod);
		} catch(\Usluz\Core\Error\Data\DataINIConfig $e) {
			throw new Error\UnregistredProcessClass("Method $method or submethod $submethod is not properly registred");
		}

		self::check_api($values, $pconf);

		return Trigger::OK;
	}

	static private function check_api(array &$values, array &$template) {
		$missing = [];
		foreach($template as $key => &$val) {
			$type = NULL;
			switch($val['type']) {
				case 'integer':
					$type = FILTER_VALIDATE_INT; break;
				case 'float':
				case 'double':
					$type = FILTER_VALIDATE_FLOAT; break;
				case 'boolean':
					$type = FILTER_VALIDATE_BOOLEAN; break;
				case 'string':
					continue 2; // because break == continue (Grrrr)
				default:
					throw new Error\UnsupportedType("Unsupported type: " . $val['type']);
			}
			if(!isset($values[$key]) && !$val['required'])
				continue;
			if(!isset($values[$key]) && $val['required']) {
				$missing[] = $key;
				continue;
			}

			$_tmp = filter_var($values[$key], $type);
			if(!$_tmp)
				throw new Error\APIArgType("invalid argument type!");
			$values[$key] = $_tmp;
		}
		if($missing) {
			if(count($missing) > 1)
				throw new Error\APIArgRequired("Arguments {".join($missing, ", ")."} is required");
			throw new Error\APIArgRequired("Argument '$missing[0]' is required");
		}

	}
}
