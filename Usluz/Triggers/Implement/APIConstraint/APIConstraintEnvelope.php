<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\SimpleRouter
*/

namespace Usluz\Triggers\Implement\APIConstraint;



/**
 * Obalka pro SimpleRouter :)
*/
class APIConstraintEnvelope extends \Usluz\Triggers\Abstr\AbstractEnvelope {
	public function __construct() {
		parent::__construct(new APIConstraintRequestTrigger(), NULL);
	}
} 
