<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache
*/

namespace Usluz\Triggers\Implement\Cache;
use \Usluz\Core\Comm\Trigger;
use \Usluz\Core\Other\Cache;
use \Usluz\Core\Other;
use \Usluz\Core;
use \Usluz\Triggers\Abstr;



/**
 *
*/
class CacheRequestTrigger extends Abstr\AbstractRequestTrigger {
	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function get_name() {
		return 'Cache';
	}

	public function get_dependices() {
		return [
			'SimpleRouter',
			'APIConstraint'
		];
	}



	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function execute(\Usluz\Core\Other\Conf\Conf $conf, \Usluz\Core\Storage\Storage &$request, \Usluz\Core\Storage\Storage $server) {

		$check_request = self::check_request($request);
		if($check_request) {
			$max_age     = $conf->get_value('default_max_age');
			$dir         = 'cache';
			$key         = Cache\Cache::genere_key($request->get_values(), $dir, $max_age);
			$check_cache = Cache\Cache::check($key);

			$local_storage = &$this->get_local_storage();
			$local_storage->set_value('cache', (int)$check_request);
			$local_storage->set_value('key', $key);

			if(!$check_cache) {
				$local_storage->set_value('cache_valid', 0);
				return Trigger::OK;
			}

			$local_storage->set_value('cache_valid', 1);
			return Trigger::BREAK_AT_END;
		}

		return Trigger::OK;
	}

	/**
	 * Zkontroluje, zdali dany Request povoluje Cache
	*/
	static private function check_request(&$request) {
		$meta = &$request->get_metadata();
		if((!isset($meta['_cache_enabled'])) || (!$meta['_cache_enabled']))
			return false;
		return true;
	} 
}

Core\GlobalDir::add('cache', 0770);
Core\GlobalCron::add(3600,
	function() {
		$dir = Core\GlobalConf::get("TEMPORARY_DIR") . '/cache';
		$content = scandir($dir);
		foreach($content as &$val) {
			if($val === "." || $val === "..")
				continue;
			if(filemtime($dir . "/" . $val) < time())
				unlink($dir . "/" . $val);
		}
	}
);
