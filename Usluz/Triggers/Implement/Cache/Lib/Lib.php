<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache\Lib
*/

namespace Usluz\Triggers\Implement\Cache\Lib;



/**
 * Mala knihovnicka funkci pro Cache
*/
class Lib {
	/**
	 * Vygeneruje klic, pod kterym bude dany objek "zacachovan"
	 * @param array $request_values
	 * @return string
	*/
	static public function genere_key(\Usluz\Core\Storage\Storage &$request) {
		$request_values = &$request->get_values();
		ksort($request_values);
		$keys   = array_keys($request_values);
		$values = array_values($request_values); 
		return md5(join('', $keys) . join('', $values));
	}

	/**
	 * Otevre soubor pro cteni a zapis :).
	 * @param  string $path
	 * @return file_descriptor
	*/
	static public function open_file($path) {
		return fopen($path, 'a+');
	}

	/**
	 * Zavre file_desriptor
	 * @return
	*/
	static public function close_file($file_descriptor) {
		return fclose($file_descriptor);
	}

	/**
	 * Zapise objekt Storage bezpecne do souboru
	 * @param INT file_descriptor
	 * @param \Usluz\Core\Storage\Storage
	 * @return Navratovy kod funkce fwrite($file_descriptor...)
	*/
	static public function write($file_descriptor, \Usluz\Core\Storage\Storage &$storage) {
		return fwrite($file_descriptor, serialize($storage));
	}

	/**
	 * Nacte objekt Storage z souboru, na ktery ukazuje file_descriptor
	 * (nemeni jeho "pozici" v soubotu pred zapocetim cteni)
	 * @param int file_descriptor
	 * @return \Usluz\Core\Storage\Storage|false
	*/
	static public function read($file_descriptor) {
		$_tmp = '';
		while(!feof($file_descriptor)) {
			$data = fread($file_descriptor, 1024);
			$_tmp .= $data;
		}

		return unserialize($_tmp);
	}
}
