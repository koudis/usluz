<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache
*/

namespace Usluz\Triggers\Implement\Cache;



/**
 * Obalka pro Cache :)
*/
class CacheEnvelope extends \Usluz\Triggers\Abstr\AbstractEnvelope {
	public function __construct() {
		parent::__construct(new CacheRequestTrigger(), new CacheResponseTrigger());
	}
} 
