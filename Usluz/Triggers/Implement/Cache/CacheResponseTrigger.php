<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache
*/

namespace Usluz\Triggers\Implement\Cache;
use \Usluz\Core\Comm\Trigger;
use \Usluz\Core\Other\Cache;



/**
 *
*/
class CacheResponseTrigger extends \Usluz\Triggers\Abstr\AbstractResponseTrigger {
	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	public function get_name() {
		return 'Cache';
	}

	public function get_dependices() {
		return array();
	}

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceResponseTrigger
	*/
	public function execute(\Usluz\Core\Other\Conf\Conf $conf, \Usluz\Core\Storage\Storage &$response) {
		$local_storage = &$this->get_local_storage();

		if(!$local_storage->get_value('cache'))
			return Trigger::OK;

		$fe   = $conf->get_config('file_extension');
		$key  = $local_storage->get_value('key');
		$enabled = ($local_storage->is_set('cache') && $local_storage->get_value('cache'));
		$valid   = ($local_storage->is_set('cache_valid') && $local_storage->get_value('cache_valid'));

		if($valid) {
			$response = Cache\Cache::load($key);
			return Trigger::OK;
		}

		if ($enabled) {
			Cache\Cache::write($key, $response);
			return Trigger::OK;
		}
		
		return Trigger::OK;
	}
}

