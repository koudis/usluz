<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\SimpleRouter
*/

namespace Usluz\Triggers\Implement\SimpleRouter;
use \Usluz\Core\Comm\Trigger;



/**
 *
*/
class SimpleRouterRequestTrigger extends \Usluz\Triggers\Abstr\AbstractRequestTrigger {
	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function get_name() {
		return 'SimpleRouter';
	}

	public function get_dependices() {
		return array();
	}

	/**
	 * @see \Usluz\Core\Comm\Iface\InterfaceRequestTrigger
	*/
	public function execute(\Usluz\Core\Other\Conf\Conf $conf, \Usluz\Core\Storage\Storage &$request, \Usluz\Core\Storage\Storage $server) {
		$method    = $request->get_value('method_type');
		$submethod = $request->get_value('submethod_type');

		if($conf->exist($method)) {
			$m_t = $conf->get_config($method);
			if(!is_string($m_t))
				throw new Error\Data\DataINIBadFormat('key ' . $method . ' is not a string!');

			$request->set_value('method_type', $m_t);
		}
		if($conf->exist($submethod)) {
			$m_t = $conf->get_config($submethod);
			if(!is_string($m_t))
				throw new Error\Data\DataINIBadFormat('key ' . $submethod . ' is not a string!');

			$request->set_value('submethod_type', $m_t);
		}

		return Trigger::OK;
	}
}
