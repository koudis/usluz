<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\SimpleRouter
*/

namespace Usluz\Triggers\Implement\SimpleRouter;



/**
 * Obalka pro SimpleRouter :)
*/
class SimpleRouterEnvelope extends \Usluz\Triggers\Abstr\AbstractEnvelope {
	public function __construct() {
		parent::__construct(new SimpleRouterRequestTrigger(), NULL);
	}
} 
