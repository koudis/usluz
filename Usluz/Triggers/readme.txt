[Directory structure]
	Abstr/
		...
	Implement/
		<trigger_name>/
			[bin/]
			[Error/]
			<trigger_name>Envelope.php
			[<trigger_name>ResponseTrigger.php]
			[<trigger_name>RequestTrigger.php]



[Config file section]
	comm:
		triggers: <--
			<trigger_name>:
				envelope: <envelope class name with namespace>
				enabled:  <true|false>   // is trigger enabled?
				[priority: <integer>]    // not used now
				data: <data passed to trigger when executed>
				



[Response|Request class structure]
	get_name() - return a valid trigger name.
		Name will be matched by key in config.
		If there is no key with name 'get_name()' in config file,
		error occured.
	get_dependices() - return list of dependices on execute contex
		[not used now]
	execute(...) - execute trigger.
		For full signature @see Core\Comm\Iface.
		From trigger, we can control next trigger execute order,
		(for more information, @see Core\Comm\Abstr\Trigger\[Response|Request])

	[Local storage]
		Sometime is useful pass some data from request to response trigger.
		For this purpose, there is a local storage feature.
		@see Triggers\Implement\Cache\CacheRequestTrigger

	- Every trigger must implement Abstr\TriggerEnvelope
