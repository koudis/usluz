<?php
/**
 * @author Ignum
 * @package Usluz\ProcessInstances\Get
*/

namespace ProcessInstances\Get;
use \Usluz\Core\Storage;




/**
 * 
*/
class HelloWorld extends \Usluz\Core\Process\Get {

	/**
	 * @var \Usluz\Core\Storage\Storage
	*/
	private $data = null;



	public function __construct() {
		$this->data = new Storage\Storage();
		$this->register_cron(100, function() {
			echo "Cron in action :).";
		});
	}



	protected function prepare() {
		$data    = $this->comm->get_request_storage();
		$values  = &$data->get_values();
		$conf    = &$this->get_conf();

		$_tmp = array(
			'message' => $values['message']
		);
		sleep(1);
		$this->data->set_values($_tmp);
	}

	public function result() {
		return $this->data;
	}
}
