<?php
/**
 * @package Usluz\Lib
 * @author Ignum 
 */

namespace Usluz\Lib;



/**
 * Stara se o radne nacteni vsech komponen obsazinych v knihovne
*/
class Lib implements \Usluz\Core\Other\Iface\Init {
	/**
	 * @see \Usluz\Core\Others\Iface\Init
	*/
	static public function init() {
		set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . DIRECTORY_SEPARATOR . 'usluz-client');

		spl_autoload_register(function($class_name) {
			$matches = [];
			if(!preg_match('/^Symfony\\\\Component\\\\Yaml\\\\(.+)/', $class_name, $matches))
				return false;
			$name = preg_replace('/\\\\/', '/', $matches[1]);
			require_once 'symfony-yaml-parser/' . $name . '.php';
		});

		return true;
	}
}
