<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Abstr
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Abstr;



/**
 * Abstraktni trida pro \Storage 
*/
class AbstractStorage extends \Usluz\Core\Other\Object\ObjectCopy {

	/**
	 * @var $storage
	*/
	private $values = array();

	/**
	 *
	*/
	private $metadata = array();

	/**
	 *
	*/
	private $sorted_keys = array();



	public function __construct() {
	} 



	/**
	 * Nastavi hodnotu podle klice.
	 * @param string $key
	 * @param mixed $value
	 * @return mixed
	*/
	public function set_value($key, $value) {
		$this->values[$key] = $value;
		return $value;
	}

	/**
	 * Nastavi metadata pro dane uloziste
	 * @param  array $value
	 * @return array
	*/
	public function set_metadata(array $meta) {
		$this->metadata = $meta;
		return $meta;
	}

	/**
	 * @return $value
	 * @param string $key
	 * @param string $value
	*/
	public function set_metadata_value($key, $value) {
		$this->metadata[$key] = $value;
		return $value;
	}

	/**
	 * Vrati hodnotu podle klice.
	 * @param string $key
	 * @return mixed
	*/
	public function get_value($key) {
		if(isset($this->values[$key])) {
			return $this->values[$key];
		} else {
			return '';
		}
	}

	/**
	 * Vrati metadata daneho uloziste :)
	 * @return mixed
	*/
	public function &get_metadata() {
		return $this->metadata;
	}

	/**
	 * Vrati cele uloziste jako pole/hash.
	 * @return array
	*/
	public function &get_values() {
		return $this->values;
	}

	/**
	 * Nastavi cele uloziste podle predaneho pole.
	 * @param array $values
	 * @return array
	*/
	public function set_values(array $values) {
		$this->values = $values;
		return $values;
	}

	/**
	 * Zjisti, bylali promenna jiz nastavena
	 * @param string $key
	 * @return boolean
	*/
	public function is_set($key) {
		return isset($this->values[$key]);
	}

	/**
	 * Vyprazdni dane storage
	*/
	public function reset() {
		$this->values      = array();
		$this->metadata    = array();
		$this->sorted_keys = array();
	}
}
