<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;



/**
 * Zakladni \Storage
*/
class Storage extends \Usluz\Core\Storage\Abstr\AbstractStorage {
	public function __construct(array $values = array()) {
		parent::__construct();
		$this->set_values($values);
	}
}