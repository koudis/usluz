<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;
use \Usluz\Core\Error;



/**
 * Uloziste, ktere dokaze jednoznacne 'serializovat' data.
*/
class ClearStorage extends Storage implements \Usluz\Core\Storage\Iface\InterfaceClearStorage {

	/**
	 * "Logika" obsahujici alg. pro serilizaci dat
	 * @var $logic
	*/
	private $logic = null;

	/**
	 * Defaultni logika pro serializeci dat
	 * @var $default_logic
	*/
	static private $default_logic = 'DefaultClearStorageLogic';



	/**
	 * @param string $logic_name nazev tridy s 'logikou'
	*/
	public function __construct($logic_name = '') {
		parent::__construct();

		$instance = '';
		if(!$logic_name) {
			$instance = self::$default_logic;
		} else {
			$instance = $logic_name;
		}
		$instance = 'Usluz\Core\Storage\Logic\\' . $instance;

		if(!class_exists($instance)) {
			throw new Error\EClass\NotExists('ClearStorageLogic ' . $instance . ' is not exist!');
		}

		if(!class_implements($instance, '\Usluz\Core\Storage\Iface\InterfaceClearStorageLogic')) {
			throw new Error\EClass\EInstanceOf('ClearStorageLogic ' . $instance . ' is not instance of InterfaceClearStorageLogic');
		}

		$this->logic = $instance;
	}



	/**
	 * {@inheritdoc}
	*/
	public function serialize() {
		$o = $this->logic;
		$_t = $this->get_values();
		return $o::serialize($_t);
	}
}
