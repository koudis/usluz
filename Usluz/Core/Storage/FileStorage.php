<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;
use \Usluz\Core\Error;



/**
 * FileStorage umoznuje:
 *     - postupne ukladat/odlevat data do souboru
 *     - data jsou vnimana jako jeden celek
*/
class FileStorage extends FIFOStorage implements \Usluz\Core\Storage\Iface\InterfaceFileStorage {

	/**
	 * ukazatel na file-desriptor
	 * @var file-descriptor
	*/
	private $file = null;

	/**
	 * Cesta k souboru, ktery bude slouzit jako uloziste
	 * @var string
	*/
	private $file_path = '';

	/**
	 * Kolik zaznamu jiz bylo zapsano do souboru
	 * po jeho otevreni
	 * @var int
	*/
	private $count = 0;

	/**
	 *
	*/
	private $prefix_sum = array();

	/**
	 * 
	*/
	private $mode = 2;

	/**
	 * Read
	*/
	const READ  = 1;

	/**
	 * Write
	*/
	const WRITE = 2;



	public function __construct(array $a, $file_path) {
		parent::__construct($a);
		$this->file_path = $file_path;
	}



	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceFileStorage
	*/
	public function open($mode = self::WRITE) {
		$this->mode = $mode;
		$file_path  = $this->file_path;
		$file       = null;

		$len = 0;
		if($this->mode == self::READ) {
			$file = fopen($file_path, 'r');
		}
		if($this->mode == self::WRITE) {
			$file = fopen($file_path, 'w+');
		}

		if($file === false)
			throw new Error\File\FileUnexpectedError(get_class($this) . ': Could not create/open file ' . $file_path);

		$this->file = $file;
		return true;
	}

	/**	
	 * @see \Usluz\Core\Storage\Iface\InterfaceFileStorage
	*/
	public function save_and_reset() {
		if($this->mode == self::READ)
			throw new Error\File\File(get_class($this) . ' set only for read. Could not write into...');

		$save   = json_encode($this->get_values());
		$result = fwrite($this->file, $save . "\n");
		if($result === false)
			throw new Error\File\FileUnexpectedError(get_class($this) . ': Unexpected error while writing data into file');

		$this->count += 1;
		$this->set_values(array());

		return $result;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceFileStorage
	*/
	public function close() {
		if($this->mode == self::WRITE) {
			$_count = $this->count;
			if(strlen($_count) > 4)
				throw new Error\File\File(get_class($this) . ': too many records.');
		}

		$return = fclose($this->file);
		if($return === false)			
			throw new Error\File\FileUnexpectedError(get_class($this) . ': Could not close file' );

		$this->count = 0;
		return true;
	}

	/**
	 * Vrati radek na ktery ukazuje kurzor a posune
	 * kurzor o jeden radek "dolu."
	 * @return mixed hodnotu pod "kurzorem"
	*/
	public function get_next() {
		$entry = json_decode(fgets($this->file), true);
		if($entry === null)
			return null;

		return $entry;
	}

	/**
	 * Nastavy kurzor na pozici $pos
	 * Chova se stekne jako v FIFOStorage
	 * @see \Usluz\Core\Storage\FIFOStorage
	 * @param int $pos
	 * @return int Navratobou hodnotou je vysledek 'fseek'
	*/
	public function set_cursor_position($pos) {
		$processed_lines = count($this->prefix_sum);

		if($pos <= $processed_lines) {
			$offset = $this->prefix_sum[$pos - 1];
			return fseek($this->file, $offset);
		} else {
			$must_read = $pos - $processed_lines;
			$prefix    = $processed_lines ? $this->prefix_sum[$processed_lines - 1] : 0;

			fseek($this->file, $prefix);
			for($i = $processed_lines; $i < $must_read; $i++) {
				$prefix += strlen(fgets($this->file));
				$this->prefix_sum[$i] = $prefix;
			}
			return $this->set_cursor_position($pos);
		}
	}
}