<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * ;
*/
interface InterfacePagerStorage extends InterfaceFIFOStorage {
	/**
	 * Nastavi pocet zaznamu na stranku
	 * @param  int* $items_per_page
	 * @return int* $items_per_page
	*/
	public function set_items_per_page($items_per_page);

	/**
	 * Nastavi
	 * @param  int* $pnumber
	 * @return int* $pnumber
	*/
	public function set_page_number($pnumber);

	/**
	 * Vrati aktualni stranku.
	 * @return mixed;
	*/
	public function get_page();

	/**
	 * Nahraje dany zaznam stranky do uloziste Storage
	 * @param int $number
	 * @return array Zaznam stranky vyskytujici se pod cislem $number
	*/
	public function load_page_entry($number);
}