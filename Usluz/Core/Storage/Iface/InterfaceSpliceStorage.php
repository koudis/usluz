<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * Rozhrani pro SpliceStorage
*/
interface InterfaceSpliceStorage {
	/**
	 * Slje dve Storage do jednoho. Preferuje data v $object.
	 * @param Storage|array $object
	 * @return boolean
	*/
	public function splice($object);
}