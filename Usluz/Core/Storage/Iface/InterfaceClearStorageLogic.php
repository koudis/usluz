<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * Rozhrani pro ClearStorageLogic
*/
interface InterfaceClearStorageLogic {
	/**
	 * Pro zadane pole vrati string, znehoz lze
	 * vydedukovat obsah puvodniho pole 
	 * @param  array $e pole pro 'serializaci' 
	 * @return string 
	*/
	public static function serialize(array &$e);
}