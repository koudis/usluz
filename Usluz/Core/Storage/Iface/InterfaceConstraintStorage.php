<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * Rozhrani pro ConstraintStorage
*/
interface InterfaceConstraintStorage {
	/**
	 * Prida omezeni
	 * @var string $key 
	 * @return boolean
	*/
	public function add_constraint($key);

	/**
	 * Prida omezeni
	 * @var array $keys 
	 * @return boolean
	*/
	public function add_constraints($keys);

	/**
	 * Nastavi omezeni (smaze puvodni)
	 * @var array $keys 
	 * @return array $keys
	*/
	public function set_constraints(array $keys);

	/**
	 * Zkontroluje, zdali jsou spolneny podminky omezeni
	 * @return boolean
	*/
	public function check();
}