<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * Rozhrani pro ClearStorageLogic
*/
interface InterfaceClearStorage {
	/**
	 * Vrati string, znehoz lze
	 * vydedukovat obsah puvodniho (zapouzdreneho) pole 
	 * @return string 
	*/
	public function serialize();
}