<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
 * Rozhrani pro FIFOStorage
 * (v podstate se jedna o frontu)
*/
interface InterfaceFIFOStorage {
	/** 
	 * Proda dalsi element do fronty
	 * @param  mixed $value
	 * @return $value
	*/
	public function add($value);

	/**
	 * Vrati dalsi hodnotu z pole
	 * @return mixed
	*/
	public function get_next();

	/**
	 * Nastavi ukazatel na pozici $pos v poli (pos = index)
	 * get_next vrati hodnotu, ne kterou ukazuje $pos 
	 * @param  int $pos
	 * @return ;
	*/
	public function set_cursor_position($pos);
}