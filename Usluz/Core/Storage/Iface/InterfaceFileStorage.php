<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Iface
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Iface;



/**
*/
interface InterfaceFileStorage extends InterfaceFIFOStorage {
	/**
	 * Otevre soubor, predstavujici uloziste pod danym modem (WRITE nebo READ) 
	 * @param  string $mode
	 * @return true
	*/
	public function open($mode);

	/**
	 * Zapise vsechna data obsazena v tride jako jeden
	 * celek a po zapisu je z tridy smaze
	 * @return boolean (true - OK, false - :()
	*/
	public function save_and_reset();

	/**
	 * Uzavre soubor.
	 * @return boolean
	*/
	public function close();
}