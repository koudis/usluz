<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;
use \Usluz\Core\Other\Help;
use \Usluz\Core\Error;



/**
 * Storage umoznujici vyzadavani zadanycklicu 
*/
class SpliceStorage extends Storage implements \Usluz\Core\Storage\Iface\InterfaceSpliceStorage {
	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceSpliceStorage
	*/
	public function splice($obj) {
		$_v = &$this->get_values();
		$_a = array();
		if(is_array($obj)) {
			$_a = &$obj;
		}
		else if($obj instanceof \Usluz\Core\Storage\Abstr\AbstractStorage) {
			$_a = &$obj->get_values();
		} else {
			throw new Error\EClass\EInstanceOf('SpliceStroage: BAD type');
		}

		$result = $this->splice_merge_function($_v, $_a);
		$this->set_values($result);
	}

	/**
	 *
	*/
	protected function splice_merge_function(&$arr_1, &$arr_2) {
		return Help\Arr::array_merge_tree($arr_1, $arr_2);
	}
}
