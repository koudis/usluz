<?php
/**
 * @package Usluz\\Usluz\Core\Storage\Abstract
 * @author Ignum 
*/

namespace Usluz\Core\Storage\Logic;
use \Usluz\Core\Storage\Iface as Iface;



/**
 * Defaultni implementace serializace
*/
class DefaultClearStorageLogic implements Iface\InterfaceClearStorageLogic {
	/**
	 * {@inheritDoc}
	*/
	public static function serialize(array &$e) {
		return serialize($e);
	}
}