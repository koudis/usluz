<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;
use \Usluz\Core\Error;



/**
 * Tento storage by mel fungovat jako fronta
*/
class FIFOStorage extends Storage implements \Usluz\Core\Storage\Iface\InterfaceFIFOStorage {

	/**
	 * Ciselnik indexu v Strage
	 * @var array
	*/
	private $queue = array();

	/**
	 * Uklada, ktery index se jiz vyzkytuje v $queue
	 * @var array
	*/
	private $ik_queue = array();

	/**
	 * Ukazatel do $queue
	 * @var int
	*/
	private $cursor = 0;



	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceFIFOStorage
	*/
	public function add($value) {
		$index = count($this->queue);
		$this->ik_queue[$index] = $index;
		$this->queue[]          = $index;	
		return $this->set_value($index, $value);
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceFIFOStorage
	*/
	public function get_next() {
		$c = $this->cursor;
		if($c > count($this->queue))
			return null;

		$v = &$this->get_values();
		$this->cursor += 1;

		return $v[$this->queue[$c]];
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceFIFOStorage
	*/
	public function set_cursor_position($pos) {
		$this->cursor = $pos;
		return $pos;
	}



	/**
	 * Prekryva funkci set_value a prizpusobuje ji FIFO
	 * @see \Usluz\Core\Storage\Storage::set_value
	*/
	public function set_value($key, $value) {
		if(!isset($this->ik_queue[$key])) {
			$this->queue[]        = $key;
			$this->ik_queue[$key] = count($this->queue) - 1;
		}
		return parent::set_value($key, $value);
	}

	/**
	 * Prekryva set_values a prizpusobuje ji FIFO
	 * @see \Usluz\Core\Storage\Storage::set_values
	*/
	public function set_values(array $v) {
		$this->queue = array_keys($v);
		sort($this->queue);

		$this->ik_queue = array_flip($this->queue);
		$this->cursor   = 0;
		return parent::set_values($v);
	}
}