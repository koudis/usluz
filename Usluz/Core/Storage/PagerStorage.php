<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;
use \Usluz\Core\Error;



/**
 * Trida, ktera dokaze rozdelit data do vice skupinek (stranek)
 * a ty pote zpracovat.
 * (Predpokladame, ze data jsou ulozena v standardnim poli)
*/
class PagerStorage extends \Usluz\Core\Storage\FileStorage implements \Usluz\Core\Storage\Iface\InterfacePagerStorage {

	/**
	 * Pocet zaznamu na stranku
	 * @var int
	*/
	private $items_per_page = 1;

	/**
	 * Cislo stranky...
	 * @var int
	*/
	private $page_number = 0;

	/**
	 * 
	 * @var array
	*/
	private $act_page = null;


	/**
	 * @see \Usluz\Core\Storage\Iface\InterfacePagerStorage
	*/
	public function __construct(array $a, $file_path, $items_per_page = 1) {
		parent::__construct($a, $file_path);
		$this->items_per_page = $items_per_page;
	}



	/**
	 * @see \Usluz\Core\Storage\Iface\InterfacePagerStorage
	*/
	public function set_items_per_page($items_per_page) {
		$this->items_per_page = $items_per_page;
		$this->act_page       = null;
		return $items_per_page;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfacePagerStorage
	*/
	public function set_page_number($pnumber) {
		if($pnumber < 0)
			throw new Error\Data\DataConstraint(get_class($this) . ': page_number must be greater than or equal to zero!');
		$this->act_page    = null;
		$this->page_number = $pnumber;
		return $pnumber;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfacePagerStorage
	*/
	public function get_page() {
		if(is_null($this->act_page))
			$this->load_page();

		return $this->act_page;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfacePagerStorage
	*/
	public function load_page_entry($number) {
		if($number > $this->items_per_page - 1)
			throw new Error\Data\DataConstraint(get_class($this) . ': page_entry number is too high (' . $number . ' > ' . $this->items_per_page . ' - 1 )');

		$act_page = $this->get_page();
		if(is_null($act_page))
			return null;

		return $this->set_values($act_page[$number]);
	}

	/**
	 * Vrati danou stranku ve formatu:
	 * array(
	 *     0 => <prvni_zaznam>,
	 *     1 => <druhy_zaznam>,
	 *     2 => <treti_zaznam>,
	 *     ...
	 * )
	 * @return array
	*/
	protected function load_page() {
		$pn     = $this->page_number;
		$ipp    = $this->items_per_page;
		$cursor = $pn * $ipp;
		$_tmp   = null;
		$this->set_cursor_position($cursor);
		for($i = 0; $i < $ipp; $i++) {
			$entry = $this->get_next();
			if(is_null($entry))
				break;
			$_tmp[] = $entry;
		}

		return $this->act_page = $_tmp;
	}
}
