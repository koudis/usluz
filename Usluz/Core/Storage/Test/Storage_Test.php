<?php

namespace Usluz\Core\Storage\Test;

$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';



class Storage_Test extends \PHPUnit_Framework_TestCase {

	private $storage = null;



	public function __construct() {
		$this->storage = new \Usluz\Core\Storage\Storage();
	}



	public function testDefaultValue() {
		$this->assertEmpty($this->storage->get_values());
		return $this->storage;
	}

	/**
	 * @depends testDefaultValue
	*/
	public function testSetValue($c) {
		$this->assertEquals('a-a-a', $c->set_value('aaa', 'a-a-a'));
		$this->assertEquals('b-b-b', $c->set_value('bbb', 'b-b-b'));
		$c->set_value('array', array(1, 2, 3, 4, 5, 6));

		return $c;
	}

	/**
	 * @depends testSetValue
	*/
	public function testSerialize($c) {
		$v = $c->set_value('bbb', array('a' => 1, 'b' => 'x'));
		$this->assertContains(1, $v);
		$this->assertContains('x', $v);
		$this->assertArrayHasKey('a', $v);
		$this->assertArrayHasKey('b', $v);
		$this->assertCount(2, $v);
		return $c;
	}

	/**
	 * @depends testSetValue
	*/
	public function testGetValue_aaa($c) {
		$this->assertEquals('a-a-a', $c->get_value('aaa'));
		return $c;
	}

	/**
	 * @depends testSetValue
	*/
	public function testGetValue_ARRAY($c) {
		$_v = $c->get_values();
		$this->assertArrayHasKey('array', $_v);
		$this->assertEquals(1, $_v['array'][0]);
		$this->assertEquals(5, $_v['array'][4]);
		$this->assertEquals(3, $_v['array'][2]);
		$this->assertEquals(6, count($_v['array']));
		return $c;
	}


}