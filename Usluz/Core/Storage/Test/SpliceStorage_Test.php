<?php

namespace Usluz\Core\Storage\Test;

$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';



class SpliceStorage_Test extends Storage_Test  {

	public function createSpliceStorage() {
		return new \Usluz\Core\Storage\SpliceStorage();
	}

	public function testSplice() {
		$s  = $this->createStorage();
		$ss = $this->createSpliceStorage();

		$s->set_values(array(
			'a' => array('b' => 1),
			'c' => array('d' => array(1 => 'blb')),
			't' => 2
		));
		$ss->set_values(array(
			'a' => array('b' => 1),
			'x' => array(1),
			'c' => 1,
			't' => 2
		));

		$ss->splice($s);

		$v = $ss->get_values();
		$this->assertEquals(array('b' => array(1, 1)), $v['a']);
		$this->assertEquals(array(1), $v['x']);
		$this->assertEquals(array(2, 2), $v['t']);

		$_tmp = array('d' => array(1 => 'blb'), '0' => 1);
		$this->assertEquals($_tmp, $v['c']);
	}
}