<?php

namespace Usluz\Core\Storage\Test;

$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';



class ClearStorage_Test extends Storage_Test {

	public function CreateClass_default() {
		$t = new \Usluz\Core\Storage\ClearStorage();
		return $t;
	}

	public function CreateClass_json() {
		$t = new \Usluz\Core\Storage\ClearStorage('JSON');
		return $t;
	}


	/**
     * @expectedException \Usluz\Core\Error\Abstr\AbstractErrorPackage
    */
	public function testCreateClass_fail() {
		$t = new \Usluz\Core\Storage\ClearStorage('sddasddasdssasdas');
	}

	public function testSerialize_default() {
		$c = $this->CreateClass_default();
		$c->set_value('aaa', 'a-a-a');
		$c->set_value('bbb', 'b-b-b');
		$c->set_value('ccc', array(1, 2, 3, 4, 5, 6, 7, 8));

		$a = $c->get_values();
		
		$v = $c->serialize();
		$i = \Usluz\Core\Storage\Logic\DefaultClearStorageLogic::serialize($a);
		$this->assertEquals($v, $i);

		return $c;
	}

	public function testSerialize_json() {
		$c = $this->CreateClass_json();
		$c->set_value('aaa', 'a-a-a');
		$c->set_value('bbb', 'b-b-b');
		$c->set_value('ccc', array(1, 2, 3, 4, 5, 6, 7, 8));

		$a = $c->get_values();
		
		$v = $c->serialize();
		$i = \Usluz\Core\Storage\Logic\JSON::serialize($a);
		$this->assertEquals($v, $i);
	}
}