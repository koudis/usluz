<?php

namespace Usluz\Core\Storage\Test;

$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';



class FileStorage_Test extends FIFOStorage_Test  {

	public function createFIFOStorage() {
		return new \Usluz\Core\Storage\FIFOStorage();
	}

	public function testSetAdd() {
		$storage = $this->createFIFOStorage();
		$this->assertEquals('prvni_pokus', $storage->add('prvni_pokus'));
		$this->assertEquals('treti_pokus', $storage->add('treti_pokus'));
		$storage->add('druhy_pokus');

		return $storage;
	} 

	public function testSetFIFOValue() {
		$storage = $this->createFIFOStorage();
		$this->assertEquals('prvni_pokus', $storage->set_value('a', 'prvni_pokus'));
		$storage->set_value('b', 'druhy_pokus');
		$this->assertEquals('treti_pokus', $storage->set_value('c', 'treti_pokus'));

		return $storage;
	}

	public function testSetFIFOValues() {
		$storage = $this->createFIFOStorage();
		$ret = $storage->set_values(array(
			'a' => 'prvni_pokus',
			'b' => 'druhy_pokus',
			'c' => 'treti_pokus',
		));

		$this->assertEquals(array(
			'a' => 'prvni_pokus',
			'b' => 'druhy_pokus',
			'c' => 'treti_pokus',
		), $ret);

		return $storage;
	}

	/**
	 * @depends testSetAdd
	*/
	public function testSetCursorPosition($storage) {
		$this->assertEquals(1, $storage->set_cursor_position(1));
		$this->assertEquals('treti_pokus', $storage->get_next());

		$this->assertEquals(0, $storage->set_cursor_position(0));
		$this->assertEquals('prvni_pokus', $storage->get_next());

		$this->assertEquals(2, $storage->set_cursor_position(2));
		$this->assertEquals('druhy_pokus', $storage->get_next());

		return $storage;
	}

	/**
	 * @depends testSetFIFOValue
	*/
	public function testSetCursorPositionAfterSetValue($storage) {
		$this->assertEquals(1, $storage->set_cursor_position(1));
		$this->assertEquals('druhy_pokus', $storage->get_next());

		$this->assertEquals(0, $storage->set_cursor_position(0));
		$this->assertEquals('prvni_pokus', $storage->get_next());

		$this->assertEquals(2, $storage->set_cursor_position(2));
		$this->assertEquals('treti_pokus', $storage->get_next());

		return $storage;
	}

	/**
	 * @depends testSetFIFOValues
	*/
	public function testSetCursorPositionAfterSetValues($storage) {
		$this->assertEquals(1, $storage->set_cursor_position(1));
		$this->assertEquals('druhy_pokus', $storage->get_next());

		$this->assertEquals(0, $storage->set_cursor_position(0));
		$this->assertEquals('prvni_pokus', $storage->get_next());

		$this->assertEquals(2, $storage->set_cursor_position(2));
		$this->assertEquals('treti_pokus', $storage->get_next());

		return $storage;
	}
}