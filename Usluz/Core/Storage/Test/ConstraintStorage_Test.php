<?php

namespace Usluz\Core\Storage\Test;

$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';



class ConstraintStorage_Test extends SpliceStorage_Test {



	public function createStorage() {
		return new \Usluz\Core\Storage\ConstraintStorage();
	}





	public function testAddConstraint() {
		$s  = $this->createStorage();
		$_t = array(
			'a' => array(
				'a_2' => array(
					'c_1' => 1
				),
			),
			'b' => array(
				'b_2' => '/[0-9]{2,}-aa/'
			),
			'd' => array(
				'd' => '/[0-9]{2,}-aa/'
			),
		);
		$_t2 = array(
			'1' => array(
				'2' => 1
			)
		);

		$this->assertEquals(count($_t), count($s->set_constraints($_t)));
		$this->assertEquals(true, $s->add_constraints($_t2));
		return $s;
	}

	/**
	 * @depends testAddConstraint
	*/
	public function testAddData($s) {		
		$this->assertEquals(4, count(
			$s->set_value('a', array(
				'a_2' => array(
					'c_1' => 1
				),
				'd' => array(
					'd' => '12345-aa'
				),
				'b' => array(
					'b_2' => '10005-aa'
				),
				'1' => array(
					'2' => 1
				)
			))
		));

		$s->set_value('b', array('b_2' => '121311-aaaa'));
		$s->set_value('d', array('d' => '121311-aaaa'));
		$s->set_value('1', array('2' => '121311-aaaa'));

		return $s;
	}

	/**
	 * @depends testAddData
	*/
	public function testContraints($s) {
		$this->assertEquals(true, $s->check());

		$s->set_value('b', array('b_2' => 'dsddasdsadfggghh8+'));
		$this->assertEquals(false, $s->check());

		$s->set_value('1', array('s' => 1));
		$this->assertEquals(false, $s->check());

		return $s;
	}

}