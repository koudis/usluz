<?php
/**
 * @package Usluz\\Usluz\Core\Storage
 * @author Ignum 
*/

namespace Usluz\Core\Storage;



/**
 * Storage umoznujici vyzadavani zadanycklicu 
*/
class ConstraintStorage extends SpliceStorage implements \Usluz\Core\Storage\Iface\InterfaceConstraintStorage {
	/**
	 * Seznam vyzadovannych klicu
	 * @var array
	*/
	private $require = array();

	/**
	 * @var int
	*/
	const MANDATORY = 1;

	/**
	 * Zatim nepouzivane :) - navrh do budoucna
	 * @var int
	*/
	const FORBIDDEN = 0;



	public function __construct(array $values = array(), $constraint = array()) {
		parent::__construct($values);
		$this->require = $constraint;
	}



	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceConstraintStorage::add_constraint
	*/
	public function add_constraint($key) {
		$this->require[$key] = 1;
		return true;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceConstraintStorage::add_constraints
	*/
	public function add_constraints($keys) {
		$this->require = \Usluz\Core\Other\Help\Arr::array_merge_tree($this->require, $keys);
		return true;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceConstraintStorage::set_constraints
	*/
	public function set_constraints(array $c) {
		$this->require = $c;
		return $c;
	}

	/**
	 * @see \Usluz\Core\Storage\Iface\InterfaceConstraintStorage::check
	*/
	public function check() {
		if(count($this->require)) {
			$values = &$this->get_values();
			return $this->_check($values, $this->require);
		} else {
			return true;
		}
	}

	/**
	 * Rekne, zdali existuje cesta TR ve v stromu A.
	 * @param array &$a
	 * @param array &$tr
	 * @return boolean 
	*/
	private function _check(array &$a, array &$tr) {
		foreach($tr as $_k => $_v) {
			if(!isset($a[$_k])) {
				return false;
			}
			if(isset($a[$_k]) && is_array($a[$_k])) {
				if(is_array($_v)) {
					$result = $this->_check($a[$_k], $_v);
					if(!$result) {
						return false;
					}
				}
			}
			if(isset($a[$_k]) && (!is_array($a[$_k]) && is_array($_v))) {
				return false;
			}
			if(isset($a[$_k]) && (!is_array($a[$_k]) && (!is_array($_v)))) {
				if(is_string($_v)) {
					$result = preg_match($_v, $a[$_k]);
					if(!$result) {
						return false;
					}
				} else if(is_integer($_v)) {
					switch($_v) {
						case self::MANDATORY:
							return true;
					}
				}
			}
		}

		return true;
	}
}
