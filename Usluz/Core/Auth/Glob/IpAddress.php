<?php
/**
 * @package Usluz\\Usluz\Core\Auth\Glob
 * @author Ignum 
*/

namespace Usluz\Core\Auth\Glob;



/**
 * Umozni pristup podle ip adresy 
*/
class IpAddress extends \Usluz\Core\Auth\Abstr\AbstractGlobalAuth {
	/**
	 * Seznam "globalnich" prav
	 * @var array
	*/
	private $rights = array();



	/**
	 * @see \Auth\Iface\InterfaceGlobalAuth::has_right
	*/
	public function has_right($right) {
		return isset($this->rights[$right]);
	}

	/**
	 * @see \Auth\Iface\InterfaceGlobalAuth::get_rights
	*/
	public function get_rights() {
		return $this->rights;
	}

	/**
	 * @see \Auth\Iface\InterfaceGlobalAuth::validate
	*/
	public function validate() {
		parent::validate();
		$list   = $this->get_conf()->get_values();
		$server = $this->comm->get_request_server();

		$request_ip = $server->get_value('REQUEST_IP');
		foreach($list['data'] as $net) {
			if(preg_match('%^'.$net.'%', $request_ip))
				return true;
		}

		return false;
	}
}
