<?php
/**
 * @package Usluz\\Usluz\Core\Auth\Glob
 * @author Ignum 
*/

namespace Usluz\Core\Auth\Glob;



/**
 * 'storage' pro Errors...
*/
class All extends \Usluz\Core\Auth\Abstr\AbstractGlobalAuth {
	/**
	 * @see \Auth\Iface\InterfaceGlobalAuth::has_right
	*/
	public function has_right($right) {
		return true;
	}

	/**
	 * @see \Auth\Iface\InterfaceGlobalAuth::get_rights
	*/
	public function get_rights() {
		return array();
	}
}
