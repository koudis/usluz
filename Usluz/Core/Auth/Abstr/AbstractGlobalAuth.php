<?php
/**
 * @package Usluz\\Usluz\Core\Auth\Abstr
 * @author Ignum 
*/

namespace Usluz\Core\Auth\Abstr;
use \Usluz\Core\Error;



/**
 * Abstraktni trida pro "globalni" autentizaci
*/
abstract class AbstractGlobalAuth extends \Usluz\Core\Other\Conf\AbstractConfigurable {
	/**
	 * Vstupni data :)
	 * @var \Comm\Comm
	*/
	protected $comm = null;



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null) {
		parent::__construct($c);
		$this->get_conf()->add_constraint([
			'enabled' => 1
		]);
	}



	/**
	 * Vrati seznam pridelenych prav
	 * @return array
	*/
	abstract public function get_rights();

	/**
	 * Zjisti, zdali bylo prideleno pravo $right
	 * @param string $right
	 * @return boolean
	*/
	abstract public function has_right($right);

	/**
	 * Byli splneny nejnutnejsi prava proto, abychom mohli pokracovat?
	 * @return boolean
	*/
	public function validate() {
		$this->get_conf()->validate();
		$_conf = $this->get_conf()->get_values();
		if(!$_conf['enabled'])
			throw new Error\Auth\AuthTypeDisabled('Auth type disabled');

		return true;
	}

	/**
	 * "Inject" funkce pro storage
	 * @param \Storage\Storage $s
	 * @return boolean
	*/
	public function inject_comm(\Usluz\Core\Comm\Abstr\AbstractComm &$c) {
		$this->comm = &$c;
	}
}
