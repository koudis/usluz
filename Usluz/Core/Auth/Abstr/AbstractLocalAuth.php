<?php
/**
 * @package Usluz\\Usluz\Core\Auth\Abstr
 * @author Ignum 
*/

namespace Usluz\Core\Auth\Abstr;



/**
 * Abstraktni trida pro "lokalni" autentizaci
*/
abstract class AbstractLocalAuth {
	/**
	 * Vstupni data :)
	 * @var \Comm\Comm
	*/
	protected $comm = null;



	public function __construct() {
	}



	/**
	 * Vrati seznam pridelenych prav
	 * @return array
	*/
	abstract public function get_rights();

	/**
	 * Zjisti, zdali bylo prideleno pravo $right
	 * @param string $right
	 * @return boolean
	*/
	abstract public function has_right($right);

	/**
	 * "Inject" funkce pro storage
	 * @param \Storage\Storage $s
	 * @return boolean
	*/
	public function inject_comm(\Usluz\Core\Comm\Abstr\AbstractComm &$s) {
		$this->comm = &$s;
	}
}
