<?php
/**
 * @package Usluz\\Usluz\Core\Auth
 * @author Ignum 
*/

namespace Usluz\Core\Auth;
use \Usluz\Core\Error;
use \Usluz\Core\Comm;
use \Usluz\Core\Other;


/**
 *
*/
final class Auth extends Other\Conf\AbstractConfigurable {
	/**
	 * Instance od global_auth class
	 * @see \Usluz\Core\Auth\Abstr\AbstractGlobalAuth
	 * @var array
	*/
	private $global_auth = null;

	/**
	 * Instance of local_auth class
	 * @see \Usluz\Core\Auth\Abstr\AbstractLocalAuth
	 * @var array
	*/
	private $local_auth = null;

	/**
	 * INstance of "Comm" class
	 * @var \Usluz\Core\Comm\Comm
	*/
	private $comm = null;

	/**
	 * List of valid rights
	 * @var array
	*/
	private $rights = [];



	/**
	 * @param string $global_auth_class
	*/
	public function __construct($conf, $global_auth_class = '') {
		parent::__construct($conf);
		$c = &$this->get_conf();
		$file = '';
		if(!$global_auth_class)
			$global_auth_class = $c->get_config('global', 'default_auth');

		$class  = $c->get_config('global', 'namespace') . '\\' . $global_auth_class;
		$config = new Other\Conf\Conf($c->get_config('global', $global_auth_class));

		$ins = Other\Help\Autoload::_new($class, $config);
		if(!($ins instanceof Abstr\AbstractGlobalAuth)) {
			throw new Error\EClass\EInstanceOf("$global_auth_class is not instance of AbstractGlobalAuth");
		}

		$this->global_auth = &$ins;
	}



	/**
	 * @return boolean
	*/
	public function has_right($right) {
		$r = $this->get_rights();
		return isset($r[$right]);
	}

	/**
	 * @return array 
	*/
	public function get_rights() {
		return ($_t = &$this->rights) ? $_t : $this->prepare_rights();
	}

	/**
	 * Nastavi lokalni auth tridu 
	*/
	public function set_local_auth(Abstr\AbstractLocalAuth $a) {
		$this->local_auth = $a;
		$this->local_auth->inject_comm($this->comm);
		$this->prepare_rights();
	}

	/**
	 * Standardni "inject" funkce pro Comm tridu
	 * @param \Usluz\Core\Comm\Comm\Abstr\AbstractComm &$c
	 * @return boolean
	*/
	public function inject_comm(Comm\Abstr\AbstractComm &$c) {
		$this->comm = &$c;
		$this->global_auth->inject_comm($c);
		return true;
	}

	/**
	 * @return boolean
	*/
	public function validate() {
		$_tmp = $this->global_auth->validate();
		if(!$_tmp) {
			throw new Error\Auth\AuthInsufficientRights('AbstractWorkflow: auth problem. Pls check request and try again');
		}

		return true;
	}



	/**
	 * "Mergne" global_auth_rights s local_auth_rights. Globalni prava maji vyssi prioritu!
	 * @return array
	*/
	private function prepare_rights() {
		$r = &$this->rights;
		$g = &$this->global_auth;
		$l = &$this->local_auth;

		foreach($g as $_k => $_v) {
			$l[$_k] = $_v;
		}

		$this->rights = $l;
		return $l;
	}
}
