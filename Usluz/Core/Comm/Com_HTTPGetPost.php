<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm;
use \Usluz\Core\Error;



/**
 * "Komunikacni" trida vyuzivajici pro komunikaci s klientem standardni implementaci HTTP protokolu v web serveru.
*/
final class Com_HTTPGetPost extends Abstr\Trigger\AbstractCommTrigger {
	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, array &$_server = NULL, array &$_post = NULL) {
		parent::__construct($c);

		$request_conf  = $c->create_conf_from('request');
		$response_conf = $c->create_conf_from('response');

		$this->request  = new \Usluz\Core\Comm\Request\HTTPPost($request_conf, $_server, $_post);
		$this->response = new \Usluz\Core\Comm\Response\HTTP($response_conf);

		$this->trigger_autoregister();
	}

	/**
	 * @see \Comm\Comm::get_error_response
	*/
	public function get_error_response() {
		return new \Usluz\Core\Comm\Response\HTTPError\HTTPError($this->response);
	}
}
