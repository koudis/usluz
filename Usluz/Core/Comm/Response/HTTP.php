<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Response
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Response;
use \Usluz\Core\Error;



/**
 *
*/
class HTTP extends \Usluz\Core\Comm\Abstr\Trigger\AbstractResponseTrigger {
	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, \Usluz\Core\Comm\Response\HTTP $http = null) {
		parent::__construct($c, $http);
	}



	/**
	 * @see \Comm\Abstr\AbstractResponse::prepare
	*/
	protected function prepare() {
		$storage = $this->get_storage();

		return self::_prepare($storage->get_values());
	}

	/**
	 * @param array &$data
	 * @return string
	*/
	static private function _prepare(array &$data) {
		$cs = new \Usluz\Core\Storage\ClearStorage('JSON');
		$cs->set_values($data);
		return $cs->serialize();
	}
} 
