<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Response\HTTPError
 * @author Ignum
*/

namespace Usluz\Core\Comm\Response\HTTPError;
use \Usluz\Core\Error;



/**
 * Obstarava mapovani nazvu chybovych trid s http response_code
 * @internal
*/
class _HTTPErrorMap {
	/**
	 * Prirazuje Error tridy k http response_code
	 * @var array
	*/
	static private $MAP = array(
		'Core\Error\Auth\AuthInsufficientRights' => 403,
		'Core\Error\Data\DataConstraint' => 400,
		'Core\Error\EClass\EInstanceOf'  => 500,
		'Core\Error\EClass\NotExists'    => 500
	);

	/**
	 * Defaultni Error response code
	 * @var integer
	*/
	static private $DEFAULT = 404;

	/**
	 * Defaultni Error response code pro "externi" chyby :)
	 * @var integer
	*/
	static private $EXTERN_DEFAULT = 500;

	/**
	 * @param string $class_name
	*/
	static public function get_response_code($class_name = '') {
		if($class_name && isset(self::$MAP[$class_name])) {
			return self::$MAP[$class_name];
		} else {
			return self::$DEFAULT;
		}
	}

	/**
	 * @param string $class_name
	*/
	static public function get_response_code_for_extern($class_name = '') {
		return self::$EXTERN_DEFAULT;
	}
}



/**
 * {@inheritdoc}
*/
class HTTPError extends \Usluz\Core\Comm\Response\HTTP implements \Usluz\Core\Comm\Iface\InterfaceErrorResponse {
	use \Usluz\Core\Comm\Trt\TraitErrorResponse;

	public function __construct(\Usluz\Core\Comm\Response\HTTP $http = null) {
		parent::__construct(null, $http);
	}



	/**
	 * @see \Comm\Abstr\AbstractResponse::process
	*/
	public function process() {
		$resp_code = 0;
		if($this->extern_error) {
			$cls = get_class($this->extern_error);
			$resp_code = _HTTPErrorMap::get_response_code_for_extern($cls);
		} else if($this->error) {
			$cls = get_class($this->error);
			$resp_code = _HTTPErrorMap::get_response_code($cls);
		} else {
			$resp_code = _HTTPErrorMap::get_response_code();
		}

		http_response_code($resp_code);
		return parent::process();
	}
}
