<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Request
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Request;
use \Usluz\Core\Error;




/**
 * Trida pro zpracovani POST pozadavku )...
 * @see \Usluz\Core\Comm\Request\HTTP\HTTP
*/
class HTTPPost extends \Usluz\Core\Comm\Request\HTTP\HTTP {
	/**
	 * Slouzi pro "rozparsovani" url pozadavku...
	 * @var \Usluz\Core\Comm\Request\HTTP\HTTP
	*/
	protected $get_class = NULL;

	/**
	 *
	 * @var array
	*/
	protected $post = array();

	/**
	 * @var uint
	*/
	const DEFAULT_GET_CLASS = 1;

	/**
	 * @var uint
	*/
	const HTTP_GET_NICE = 1;

	/**
	 * @var uint
	*/
	const HTTP_GET = 0;



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, array &$s = NULL, array &$p = NULL, $get_class_id = self::DEFAULT_GET_CLASS) {
		parent::__construct($c, $s);
		if($p)
			$this->$post = &$p;
		switch($get_class_id) {
			case self::HTTP_GET:
				$this->get_class = new HTTPGet($c, $s);
				break;
			case self::HTTP_GET_NICE:
				$this->get_class = new HTTPGetNice($c, $s);
				break;
			default:
				throw new Comm\BadRequestMethod('Ouuu, your request method is not supported');
		}
	}



	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest::process
	*/
	public function process() {
		$get_class = &$this->get_class;
		$_t = $get_class->process();
		if(!$_t)
			throw new Error\Com\Com('Fattal error in COM (HTTPPost)');

		$storage = &$get_class->get_storage();
		$this->set_storage($storage);
		$process_state = parent::process();

		$splice_storage = new \Usluz\Core\Storage\SpliceStorage($storage->get_values());
		$splice_storage->splice($this->get_storage());

		if(isset($this->post['method_type']))
			throw new Error\Comm\OverrideNotAllowed('method_type');
		if(isset($this->post['submethod_type']))
			throw new Error\Comm\OverrideNotAllowed('submethod_type');

		$splice_storage->splice($this->post);

		$storage->set_values($splice_storage->get_values());
		$this->set_storage($storage);
		return $process_state;
	}




	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest::prepare
	*/
	protected function prepare() {
		return true;
	}
}
