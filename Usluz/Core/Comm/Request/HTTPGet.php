<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Request
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Request;
use \Usluz\Core\Comm\Help\_Storage;
use \Usluz\Core\Error;




/**
 * Trida pro zpracovani GET pozadavku skrze HTTP
 * @see \Usluz\Core\Comm\Abstr\AbstractRequest
*/
class HTTPGet extends \Usluz\Core\Comm\Request\HTTP\HTTP {
	/**
	 * Oddelovac jednotlivych argumentu
	 * @var string
	*/
	const ARG_DELIM = '&';

	/**
	 * Oddelovac klic, hodnota
	 * @var string
	*/
	const KEY_VALUE_DELIM = '=';

	/**
	 * Server (v APACHE napr. $_SERVER)
	 * @var array();
	*/
	//protected $_server = array();



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, array &$s = NULL) {
		parent::__construct($c, $s);
	}



	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest::prepare
	*/
	protected function prepare() {
		$storage = &$this->get_storage();
		$storage->set_values($this->parse_url());

		return parent::prepare();
	}

	/**
	 * Rozparsuje URL :).
	 * @return array()
	*/
	protected function parse_url() {
		$url = $this->get_server()->get_value('REQUEST_URL');
		$matches = array();
		preg_match('%(\?[^/]*)$%', $url, $matches);

		if(!count($matches)) {
			return array('query_string' => '');
		}
		$qs = preg_replace('/^\?/', '', $matches[1]);

		$args = preg_split('/' . self::ARG_DELIM . '/', $qs);

		if(!count($args)) {
			return array('query_string' => $matches[1]);
		}

		_Storage::$list = array();
		array_map(function($v) {
			$_a = preg_split('/' . self::KEY_VALUE_DELIM . '/', $v);
			if(count($_a) == 2) {
				_Storage::$list[rawurldecode($_a[0])] = rawurldecode($_a[1]);
			} else if(count($_a) == 1) {
				_Storage::$list[rawurldecode($_a[0])] = null;
			}
			unset($_a);
		}, $args);

		$result = _Storage::$list;

		if(!count(_Storage::$list)) {
			throw new Error\Comm\InvalidRequestData();
		} else {
			return $result;
		}
	}
}
