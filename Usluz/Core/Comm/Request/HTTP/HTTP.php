<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Request\HTTP
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Request\HTTP;
use Core\Error;




/**
 * Trida pro zpracovani GET pozadavku skrze HTTP
 * @see \Usluz\Core\Comm\Abstr\AbstractRequest
*/
class HTTP extends \Usluz\Core\Comm\Abstr\Trigger\AbstractRequestTrigger {
	/**
	 * Server (v APACHE napr. $_SERVER)
	 * @var array();
	*/
	protected $_server = array();



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, array &$s = NULL) {
		parent::__construct($c);
		$this->_server = &$s;
		$this->set_storage(new \Usluz\Core\Storage\ConstraintStorage());
	}


	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest::prepare
	*/
	protected function prepare() {
		$storage = $this->get_storage();
		if(!$storage->check()) {
			throw new Error\Data\DataConstraint('HTTP - missing required attributes!');
		}
		return true;
	}

	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest::get_server_data
	*/
	protected function set_init_server_data() {
		$s = &$this->server;
		$s->set_value('REQUEST_URL', \Usluz\Core\Comm\Help\Url::full_url($this->_server));
		$s->set_value('REQUEST_IP', $this->_server['REMOTE_ADDR']);
		return $s;
	}
}
