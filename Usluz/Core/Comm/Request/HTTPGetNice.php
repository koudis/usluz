<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Request
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Request;
use \Usluz\Core\Comm\Help\_Storage;
use \Usluz\Core\Error;




/**
 * Trida pro zpracovani GET pozadavku skrze HTTP
 * http://<source>/method/submethod/[subsubmethod/][?(<key>=<value>)+]
 * @see \Usluz\Core\Comm\Abstr\AbstractRequest
*/
class HTTPGetNice extends HTTPGet {

	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, array &$s = NULL) {
		parent::__construct($c, $s);
	}




	/**
	 * Rozparsuje URL :).
	 * @return array()
	*/
	protected function parse_url() {
		$url  = $this->get_server()->get_value('REQUEST_URL');
		$_tmp = parent::parse_url();

		$matches = '';
		preg_match('%^(?:http[s]?://)[^/]+/([^/]*)/([^/?]*)(?:/([^/?]*))?%', $url, $matches);

		if($matches[1] && $_tmp['method_type'])
			throw new Error\Comm\OverrideNotAllowed('Override not allowed for "method_type"');
		if($matches[2] && $_tmp['submethod_type'])
			throw new Error\Comm\OverrideNotAllowed('Override not allowed for "submethod_type"');
		if($matches[3] && $_tmp['subsubmethod_type'])
			throw new Error\Comm\OverrideNotAllowed('Override not allowed for "subsubmethod_type"');

		if((!$matches[1]) && $_tmp['method_type'])
			$matches[1] = $_tmp['method_type'];
		if((!$matches[2]) && $_tmp['submethod_type'])
			$matches[2] = $_tmp['submethod_type'];
		if((!$matches[3]) && $_tmp['subsubmethod_type'])
			$matches[3] = $_tmp['subsubmethod_type'];

		if((!$matches[1]) || (!$matches[2])) {
			return array('query_string' => '');
		}

		$result = array(
			'method_type'    => $matches[1],
			'submethod_type' => $matches[2]
		);
		if(isset($matches[3]))
			$result['subsubmethod_type'] = $matches[3];

		$result = array_merge($result, $_tmp);

		return $result;
	}
}
