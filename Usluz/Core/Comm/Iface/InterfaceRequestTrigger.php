<?php
/**
 * @author Ignum
 * @package Usluz\\Usluz\Core\Comm
*/

namespace Usluz\Core\Comm\Iface;



/**
 *
*/
interface InterfaceRequestTrigger {
	/**
	 * Vrati jmeno triggru (unikatni jmeno!)
	 * @return string
	*/
	public function get_name();

	/**
	 * Zpracuje trigger... POkud vrati false, zpracovani ukonci a prejde k dalsimu kroku (auth)
	 * @param \Usluz\Core\Storage\Storage $request
	 * @param \Usluz\Core\Storage\Storage $server
	 * @return boolean
	*/
	public function execute(\Usluz\Core\Other\Conf\Conf $conf, \Usluz\Core\Storage\Storage &$request, \Usluz\Core\Storage\Storage $server);

	/**
	 * Vrati pole, ktere predstavuje mnozinu jmen triggeru (get_name),
	 * ktere musi byt zavalany pred zavolanim daneho triggeru
	 * @return array
	*/
	public function get_dependices();

	/**
	 * Slouzi splneni zavislosti vracenych funkci get_dependices.
	 * @param \Usluz\Core\Comm\Iface\InterfaceRequestTrigger $trigger
	 * @return boolean
	*/
	public function add_follow_trigger(\Usluz\Core\Comm\Iface\InterfaceRequestTrigger $trigger);
}
