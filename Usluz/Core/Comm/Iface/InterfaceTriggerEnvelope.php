<?php
/**
 * @author Ignum
 * @package Usluz\\Usluz\Core\Comm\Iface
*/

namespace Usluz\Core\Comm\Iface;



/**
 * Umoznuje propojit response a request trigger :)
*/
interface InterfaceTriggerEnvelope {
	/**
	 * ;
	 * @return \Usluz\Core\Comm\Abstr\AbstractRequestTrigger
	*/
	public function get_request_trigger();

	/**
	 * ;
	 * @return \Usluz\Core\Comm\Abstr\AbstractResponseTrigger
	*/
	public function get_response_trigger();
}
