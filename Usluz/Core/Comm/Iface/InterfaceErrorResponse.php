<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Iface;
use \Usluz\Core\Error;



/**
 * Interface pro odesilani chyb!
*/
interface InterfaceErrorResponse {
	/**
	 * Nastavi Error systemu 
	 * @param Error\Abstr\AbstractErrorPackage $e
	 * @return Error\Abstr\AbstractErrorPackage
	*/
	public function set_error(Error\Abstr\AbstractErrorPackage $e);

		/**
	 * Nastavi "externi" Error (tedy ten, ktery vetsinou vzesel z soft. tretich stran) 
	 * @param \Exception $e
	 * @return \Exception $e
	*/
	public function set_extern_error(\Exception $e);
}