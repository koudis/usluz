<?php
/**
 * @author Ignum
 * @package Usluz\\Usluz\Core\Comm
*/

namespace Usluz\Core\Comm\Iface;



/**
 * Rozhrani pro "response" odchytavace ;)
*/
interface InterfaceResponseTrigger {
	/**
	 * Vrati jmeno triggru (unikatni jmeno!)
	 * @return string
	*/
	public function get_name();

	/**
	 * Zpracuje trigger. Pokud vrati false, zpracovani se ukoncuje a request se odesle...
	 * @param \Usluz\Core\Storage\Storage &$response
	 * @return boolean
	*/
	public function execute(\Usluz\Core\Other\Conf\Conf $conf, \Usluz\Core\Storage\Storage &$response);

	/**
	 * Vrati pole, ktere predstavuje mnozinu jmen triggeru (get_name),
	 * ktere musi byt zavalany pred zavolanim daneho triggeru
	 * @return array
	*/
	public function get_dependices();

	/**
	 * Slouzi splneni zavislosti vracenych funkci get_dependices.
	 * @param \Usluz\Core\Comm\Iface\InterfaceResponseTrigger $trigger
	 * @return boolean
	*/
	public function add_follow_trigger(\Usluz\Core\Comm\Iface\InterfaceResponseTrigger $trigger);
}
