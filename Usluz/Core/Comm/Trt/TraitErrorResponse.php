<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Trt
 * @author Ignum
*/

namespace Usluz\Core\Comm\Trt;
use \Usluz\Core\Error;



/**
 *
*/
trait TraitErrorResponse {
	/**
	 * Chyba
	 * @var \Usluz\Core\Error\Abstr\AbstractErrorPackage
	*/
	protected $error = null;

	/**
	 * Chyba
	 * @var \Exception
	*/
	protected $extern_error = null;



	/**
	 * Nastavi Error systemu 
	 * @param Error\Abstr\AbstractErrorPackage $e
	 * @return Error\Abstr\AbstractErrorPackage
	*/
	public function set_error(Error\Abstr\AbstractErrorPackage $e) {
		$this->error = $e;
		return $e;
	}

	/**
	 * Nastavi "externi" Error (tedy ten, ktery vetsinou vzesel z soft. tretich stran) 
	 * @param \Exception $e
	 * @return \Exception
	*/
	public function set_extern_error(\Exception $e) {
		$this->extern_error = $e;
		return $e;
	}
}