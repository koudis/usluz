<?php
/**
 * @package Usluz\\Usluz\Core\Comm\Help
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Help;


/**
 *
*/
class Url {
	/**
	 * Vrati validni URL pozadavku
	 * @param $_SERVER $s
	 * @return string URL pozadavku
	*/
	static function full_url(&$s)
	{
		$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
		$sp  = strtolower($s['SERVER_PROTOCOL']);
		$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
		$port = $s['SERVER_PORT'];
		$port = ((!$ssl && $port) || ($ssl && $port)) ? '' : ':'.$port;
		$host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];
		return $protocol . '://' . $host . $port . $s['REQUEST_URI'];
	}
}
