<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr;


/**
 * Abstraktni response.
 * Vsechny "response" tridy musis dedit z AbstractResponse
*/
abstract class AbstractResponse extends \Usluz\Core\Other\Conf\AbstractConfigurable {
	/**
	 * @var \Usluz\Core\Storage\Storage
	*/
	private $data = null;



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, \Usluz\Core\Comm\Abstr\AbstractResponse $r = null) {
		parent::__construct($c);
		if($r) {
			$this->data = $r->get_storage();
			$this->set_conf($r->get_conf());
		}
	}



	/**
	 * Nastavi storage pro dany request
	 * @param \Storage\Storage $s
	*/
	public function set_storage(\Usluz\Core\Storage\Storage $s) {
		$this->data = $s;
		return $s;
	}

	/**
	 * Zpracuje request. Nutno volat pred get_storage!
	 * @return boolean
	*/
	public function process() {
		return $this->prepare();
	}

	/**
	 * Vlastni implementace zpracovani response.
	 * @return boolean 
	*/
	abstract protected function prepare();

	/**
	 * Vrati uloziste pro request...
	 * @return \Usluz\Core\Storage\Storage
	*/
	protected function &get_storage() {
		return $this->data;
	}
}
