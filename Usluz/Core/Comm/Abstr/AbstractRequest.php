<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr;
use \Usluz\Core\Error;



/**
 * Abstraktni request.
 * Vsechny requesty musi dedit z teto tridy! 
*/
abstract class AbstractRequest extends \Usluz\Core\Other\Conf\AbstractConfigurable {
	/**
	 * @var \Usluz\Core\Storage\ConstraintStorage
	*/
	protected $data = null;

	/**
	 * Server promenna serveru :). Pro definici
	 * viz. $_SERVER na strankach PHP
	 * @var \Usluz\Core\Storage\Storage
	*/
	protected $server = null;

	/**
	 * Seznam pozadovanych polozek pro request
	 * @var array
	*/
	static protected $REQUIRE = array(
	);

	/**
	 * Seznam pozadovanych polozek pro server promennou
	 * @var array
	*/
	static protected $REQUIRE_SERVER = array(
		'REQUEST_URL' => 1,
		'REQUEST_IP'  => 1,
	);



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null) {
		parent::__construct($c);
		$this->server = new \Usluz\Core\Storage\ConstraintStorage(static::$REQUIRE_SERVER);
		$this->data   = new \Usluz\Core\Storage\ConstraintStorage(static::$REQUIRE);
	}



	/**
	 * Vrati uloziste pro request...
	 * @return \Usluz\Core\Storage\ConstraintStorage
	*/
	public function &get_storage() {
		return $this->data;
	}

	/**
	 * Vrati informace o serveru
	 * @return \Usluz\Core\Storage\Storage 
	*/
	public function get_server() {
		return $this->server;
	}

	/**
	 * Zpracuje request. Nutno volat pred get_storage!
	 * @return boolean
	*/
	public function process() {
		$this->set_init_server_data();
		if(!$this->server->check())
			throw new Error\Data\DataConstraint("Ou, some 'server' keys missing!");

		$_tmp = $this->prepare();
		return $_tmp;
	}

	/**
	 * Vlastni implementace zpracovani request pozadavku.
	 * (napr. rozparsovani URL pro HTTP GET pozadavek)
	 * @return boolean
	*/
	abstract protected function prepare();

	/**
	 * Prida constraints pro request;
	 * @return boolean
	*/
	public function add_constraints(array $a) {
		$this->data->add_constraints($a);
		return true;
	}

	/**
	 * Nastavi storage pro dany request
	 * @param \Usluz\Core\Storage\ConstraintStorage $s
	*/
	protected function set_storage(\Usluz\Core\Storage\ConstraintStorage $s) {
		$this->data = $s;
		return $s;
	}

	/**
	 * Inicializuje server data
	 * @return \Usluz\Core\Storage\Storage
	*/
	protected function set_init_server_data() {
		return $this->server;
	}
}
