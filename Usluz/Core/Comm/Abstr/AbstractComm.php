<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr;
use \Usluz\Core\Error;



/**
 * Abstraktni trida  pro sestaveni "workflow" pro kazdy
 * pozadovany typ komunikace
*/
abstract class AbstractComm extends \Usluz\Core\Other\Conf\AbstractConfigurable {
	/**
	 * @var \Comm\Abstr\AbstractRequest
	*/
	protected $request;

	/**
	 * @var \Comm\Abstr\AbstractResponse
	*/
	protected $response;



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null) {
		parent::__construct($c);
	}



	/**
	 * Vrati data requestu
	 * @return \Storage\Storage
	*/
	public function get_request_storage() {
		return $this->request->get_storage()->cast('\Usluz\Core\Storage\Storage');
	}

	/**
	 * Vrati server data requestu
	 * @return \Storage\Storage
	*/
	public function get_request_server() {
		return $this->request->get_server();
	}

	/**
	 * 
	*/
	abstract public function get_error_response();

	/**
	 * Nastavi response storage ;).
	 * @param \Usluz\Core\Storage\Storage $s
	 * @return \Usluz\Core\Storage\Storage $s
	*/
	public function set_response_storage(\Usluz\Core\Storage\Storage $s) {
		return $this->response->set_storage($s);
	}

	/**
	 * Nastavi vyzadovane attributy pro request ;).
	 * @param array $a
	 * @return array $a
	*/
	public function add_request_constraints(array $a) {
		$this->request->add_constraints($a);
		return true;
	}

	/**
	 * Zpracujme odpoved (pripravime a odesleme)
	 * @return boolean
	*/
	public function process_response() {
		return $this->response->process();
	}

	/**
	 * Zpracuj pozadavek
	 * @return boolean
	*/
	public function process_request() {
		return $this->request->process();
	}
}
