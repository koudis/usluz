<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr\Trigger;
use \Usluz\Core\Error;
use \Usluz\Core\Comm\Abstr;
use \Usluz\Core\Comm\Trigger;
use \Usluz\Core\Comm\Iface;
use \Usluz\Core\Other\Conf;



/*
 *
*/
abstract class AbstractRequestTrigger extends Abstr\AbstractRequest {
	/**
	 * Seznam "akci" pred zpracovanim dotazu...
	 * @var array
	*/
	private $triggers = array();



	public function __construct(Conf\Conf $c = null) {
		parent::__construct($c);
	}



	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest
	*/
	public function process() {
		$_tmp = parent::process();
		if(!$_tmp) return $_tmp;

		$_tmp = $this->move_to_meta();
		if(!$_tmp) return $_tmp;

		$_trigger = $this->process_triggers();
		if(!$_trigger) return $_trigger;

		return true;
	}


	/**
	 * 
	 * @return mixed
	*/	
	public function register_trigger(Iface\InterfaceRequestTrigger $rt) {
		$this->triggers[$rt->get_name()] = $rt;
	}

	/**
	 *
	*/
	public function &get_triggers() {
		return $this->triggers;
	}

	/**
	 * Zpracuje vsechny registrovane "triggery"
	 * @return array pole zpracovanych "triggeru"
	*/
	protected function process_triggers() {
		$len  = count($this->triggers);
		$stor = &$this->get_storage();
		$conf = &$this->get_conf();

		$break_at_end = false;
		$_tmp_conf    = new Conf\Conf(); 
		foreach($this->triggers as &$t) {
			$trigger_name = $t->get_name();

			$trigger_conf = $_tmp_conf;
			if($conf->exist('triggers', $trigger_name))
				$trigger_conf->set_values($conf['triggers'][$trigger_name]['data']);

			$_tmp = $t->execute($trigger_conf, $stor, $this->get_server());
			switch($_tmp) {
				case Trigger::OK:
					break;
				case Trigger::BREAK_AT_END:
					$break_at_end = true;
					break;
				case Trigger::BREAK_NOW_SOFT:
					return Trigger::BREAK_NOW_SOFT;
				case Trigger::BREAK_NOW:
					return false;
				default:
					throw new Error\TriggerBadReturnState('Bad return state from REQUEST trigger!');
			}
		}
		if($break_at_end)
			return false;

		return true;
	}

	/**
	 * Presune data zacinajici ['meta_prefix'] z standardniho storago do
	 * jeho meta dat :).
	 * @return boolean
	 *
	*/
	private function move_to_meta() {
		$storage = &$this->get_storage();
		$values  = &$storage->get_values();
		$conf    = &$this->get_conf();

		$meta   = [];
		$prefix = $conf['meta_prefix'];
		$prefix_length = strlen($prefix);
		foreach($values as $key => &$val) {
			if(substr($key, 0, $prefix_length) == $prefix) {
				$storage->set_metadata_value($key, $val);
				unset($values[$key]);
			}
		}

		return true;
	}
}
