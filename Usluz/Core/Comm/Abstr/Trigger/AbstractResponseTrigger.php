<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr\Trigger;
use \Usluz\Core\Storage;
use \Usluz\Core\Other\Conf;
use \Usluz\Core\Comm\Abstr;
use \Usluz\Core\Comm\Trigger;
use \Usluz\Core\Comm\Iface;



/*
 *
*/
abstract class AbstractResponseTrigger extends Abstr\AbstractResponse {
	/**
	 * Seznam "akci" pred zpracovanim dotazu...
	 * @var array
	*/
	private $triggers = array();



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null, \Usluz\Core\Comm\Abstr\AbstractResponse $r = null) {
		parent::__construct($c, $r);
	}



	/**
	 * @see \Usluz\Core\Comm\Abstr\AbstractRequest
	*/
	public function process() {
		$_tmp = $this->process_triggers();
		if(!$_tmp) return $_tmp;

		return parent::process();
	}

	/**
	 * Registruje novy trigger
	 * @return boolean
	*/	
	public function register_trigger(Iface\InterfaceResponseTrigger $rt) {
		$this->triggers[$rt->get_name()] = $rt;
	}

	/**
	 *
	*/
	public function &get_triggers() {
		return $this->triggers;
	}

	/**
	 * Zpracuje vsechny registravoane triggery, v poradi v jakem byly registrovany.
	 * @return boolean (byli-li zpracovany vsechny, true. Jinak false)
	*/
	protected function process_triggers($params = array()) {
		$len  = count($this->triggers);
		$_tmp = null;
		$conf = &$this->get_conf();
		$stor = &$this->get_storage();

		if(is_null($stor))
			$stor = new Storage\Storage();

		$break_at_end = false;
		$_tmp_conf    = new Conf\Conf();
		foreach($this->triggers as &$t) {
			$trigger_name = $t->get_name();

			$trigger_conf = $_tmp_conf;
			if($conf->exist('triggers', $trigger_name)) 
				$trigger_conf->set_values($conf['triggers'][$trigger_name]['data']);

			$_tmp = $t->execute($trigger_conf, $stor);
			switch($_tmp) {
				case Trigger::OK:
					break;
				case Trigger::BREAK_AT_END:
					$break_at_end = true;
					break;
				case Trigger::BREAK_NOW_SOFT:
					return Trigger::BREAK_NOW_SOFT;
				case Trigger::BREAK_NOW:
					return false;
				default:
					throw new Error\TriggerBadReturnState('Bad return state from RESPONSE trigger!');
			}
		}
		if($break_at_end)
			return false; 

		return true;
	}
}
