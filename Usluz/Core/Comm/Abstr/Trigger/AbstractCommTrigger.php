<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm\Abstr\Trigger;
use \Usluz\Core\Other\Conf;
use \Usluz\Core\Error;
use \Usluz\Core\Comm\Abstr;
use \Usluz\Core\Comm\Iface;



/**
 * Abstraktni trida  pro sestaveni "workflow" pro kazdy
 * pozadovany typ komunikace
 * Prebira comm sekci konfigurace.
 *     comm:
 *         request|response:
 *             triggers:
 *                 <trigger_name>:
 *                     envelope: '<envelope name eith namespace>'
 *                     enabled:   0|1
 *                     [priority: <trigger priority>]
 *                     data: '[' <uzivatelska data> ']'
*/
abstract class AbstractCommTrigger extends Abstr\AbstractComm {

	/**
	 *
	 * @var array
	*/
	static private $TRIGGERS_KEY = [
		'envelope' => 1,
		'enabled'  => 1,
		'data'     => 1,
	];



	public function __construct(Conf\Conf $c = null) {
		parent::__construct($c);
		$conf = &$this->get_conf();

		$conf->splice([
			'response' => [
				'triggers' => &$conf['triggers']
			],
			'request' => [
				'triggers' => &$conf['triggers']
			]
		]);
	}



	/**
	 * @see \Usluz\Core\Comm\Abstr\Trigger\AbstractRequestTrigger
	*/
	public function register_request_trigger(Iface\InterfaceRequestTrigger $rt) {
		$this->request->register_trigger($rt);
	}

	/**
	 * @see \Usluz\Core\Comm\Abstr\Trigger\AbstractResponseTrigger
	*/
	public function register_response_trigger(Iface\InterfaceResponseTrigger $rt) {
		$this->response->register_trigger($rt);
	}

	/**
	 * @see \Usluz\Core\Comm\Abstr\Iface\InterfaceResponseTrigger
	 * @param \Usluz\Core\Comm\Iface\InterfaceTriggerEnvelope $te
	*/
	public function register_trigger_envelope(Iface\InterfaceTriggerEnvelope $te) {
		$rt = $te->get_request_trigger();
		if($rt)
			$this->register_request_trigger($rt);

		$rt = $te->get_response_trigger();
		if($rt)
			$this->register_response_trigger($rt);
	}



	/**
	 * Registruje Triggery podle konfigurace...
	 * @return boolean
	*/
	protected function trigger_autoregister() {
		$c      = &$this->get_conf();
		$trconf = &$c->create_conf_from('triggers');

		$_tmp = &$trconf->get_values();
		foreach($_tmp as $trigger_name => &$trigger) {
			$conf = $trconf->create_conf_from($trigger_name);

			if(!$conf['enabled'])
				continue;

			$t = new $conf['envelope']();

			$rt = $t->get_request_trigger();
			if($rt && $rt->get_name() != $trigger_name)
				throw new Error\Comm\TriggerMishmash("Request triggername a 'configtrigger name' jsou rozdilne!");

			$rt = $t->get_response_trigger();
			if($rt && $rt->get_name() != $trigger_name)
				throw new Error\Comm\TriggerMishmash("Response triggername a 'configtrigger name' jsou rozdilne!");

			$this->register_trigger_envelope($t);
		}

		$this->calculate_order($this->response->get_triggers());
		$this->calculate_order($this->request->get_triggers());

		return true;
	}

	/**
	 * Just calculate triggers order
	 * @param  array Triggers
	 * @return array
	*/
	protected function calculate_order(array &$triggers) {
		$map       = [];
		$step      = 10;
		$last_step = 0;
		foreach($triggers as &$t) {
			$dependices = $t->get_dependices();
			array_push($dependices, $t->get_name());
			$last_key = NULL;
			foreach($dependices as &$key) {
				if(!isset($map[$key])) {
					$last_step += $step;
					$map[$key] = $last_step;
				}
				if($last_key) {
					if(isset($map[$last_key]) && isset($map[$key])) {
						if($map[$last_key] >= $map[$key])
							throw new Error\Comm\TriggerMishmash(
								"Bad trigger arrangement - in " . $t->get_name()  . " because $last_key >= $key");
					}
					if(isset($map[$last_key]) && !isset($map[$key])) {
						$last_step = $map[$last_key] + $step;
						$map[$key] = $last_step;
					}
				}
				$last_key = $key;
			}
		}

		uksort($triggers, function($a, $b) use($map) {
			if($map[$a] == $map[$b])
				return 0;
			return ($map[$a] < $map[$b]) ? -1 : 1;
		});

		return true;
	}
}
