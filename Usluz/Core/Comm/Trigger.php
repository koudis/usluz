<?php
/**
 * @package Usluz\\Usluz\Core\Comm
 * @author Ignum 
*/

namespace Usluz\Core\Comm;



final class Trigger {
	/**
	 * Tuto konstantu vratime z Triggeru, pokud chceme zpracovani
	 * ostatnich trigerru okamzite ukoncit a vratit z funkce process_triggers
	 * false.
	 * @var int
	*/
	const BREAK_NOW = 1;

	/**
	 * Funkce process_triggers spusti vsechny registrovane Triggery.
	 * Pokud aslespon jeden Trigger vrati BREAK_NOW_SOFT, pak se zpracovani
	 * vsech nasledujicich triggeru ukonci a process_triggers vrati true.
	 * @var int
	*/
	const BREAK_NOW_SOFT = 3;

	/**
	 * Funkce process_triggers spusti vsechny registrovane Triggery.
	 * Pokud aslespon jeden Trigger vrati BREAK_AT_END pak process_triggers
	 * vrati na konci false.
	 * @var int
	*/
	const BREAK_AT_END = 2;

	/**
	 * Pokud vsechny registrovane Triggery vrati konstantu OK, pak process_triggers
	 * vrati true
	 * @var int
	*/
	const OK = 4;
}
