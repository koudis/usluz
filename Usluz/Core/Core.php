<?php
/**
 * @author jan.kubalek
*/

namespace Usluz\Core;




/**
 *
*/
class Core implements Other\Iface\Init {
	/**
	 * @see \Usluz\Core\Others\Iface\Init
	*/
	static public function init() {
		error_reporting(E_ALL & ~ E_DEPRECATED & ~ E_NOTICE & ~ E_WARNING);

		ini_set('variables_order',         'PCS');
		ini_set('iconv.input_encoding',    'UTF-8');
		ini_set('iconv.internal_encoding', 'UTF-8');
		ini_set('iconv.output_encoding',   'UTF-8');
		ini_set('default_charset',         'UTF-8');
		ini_set('mbstring.internal_encoding', 'UTF-8');
		ini_set('mbstring.http_output', 'UTF-8');

		ini_set("auto_detect_line_endings", true);

		umask(0007);

		header('X-Powered-By: USLUZ');

		return true;
	}
}


/**
 * Funkce prelozi PHP "namespace" jmeno na cestu,
 * ktera je validni pro system, v nemz byl script spusten.
*/
#spl_autoload_register(function($class_name) {
#	if(!preg_match('/^[\\\\]?Usluz[\\\\]?Core/', $class_name)) return;
#	$_name = preg_replace('/\\\\/', '/', $class_name);
#
#	if(!file_exists($_name . '.php'))
#		throw new Exception('I cannot find ' . $_name);
#
#	require_once $_name . '.php';
#});
