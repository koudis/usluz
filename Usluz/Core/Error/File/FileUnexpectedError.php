<?php
/**
 * @package Usluz\\Usluz\Core\Error\File
 * @author Ignum 
*/

namespace Usluz\Core\Error\File;



/**
 * Nastane-li pri cteni souboru k nezname chybe (nelze zapsat data na disk apod.)
*/
class FileUnexpectedError extends \Usluz\Core\Error\Abstr\AbstractErrorPackage {
	static protected $lcode = 5030;
}