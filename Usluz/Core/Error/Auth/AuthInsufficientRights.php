<?php
/**
 * @package Usluz\\Usluz\Core\Error\Auth
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Auth;



/**
 * Nedostatecna prava :)
*/
class AuthInsufficientRights extends \Usluz\Core\Error\Auth\Auth {
	static protected $lcode = 4020;
}