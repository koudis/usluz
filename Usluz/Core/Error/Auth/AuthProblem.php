<?php
/**
 * @package Usluz\\Usluz\Core\Error\Auth
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Auth;



/**
 * Authentization error (napriklad nemuzu se
 * pripojit do DB pro overeni udaju)
*/
class AuthProblem extends \Usluz\Core\Error\Auth\Auth {
	static protected $lcode = 4010;
}
