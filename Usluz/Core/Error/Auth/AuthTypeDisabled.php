<?php
/**
 * @package Usluz\\Usluz\Core\Error\Auth
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Auth;



/**
 * AuthType je vypnuty ;)
*/
class AuthTypeDisabled extends \Usluz\Core\Error\Auth\Auth {
	static protected $lcode = 4030;
}
