<?php
/**
 * @package Usluz\\Usluz\Core\Error\Process
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Process;



/**
 * Dana process trida neni registrovana :).
*/
class ProcessClassNotFound extends \Usluz\Core\Error\Process\Process {
	static protected $lcode = 6010;
}
