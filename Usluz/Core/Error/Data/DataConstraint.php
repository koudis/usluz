<?php
/**
 * @package Usluz\\Usluz\Core\Error\Data
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Data;



/**
 * Vyjimka, pokud data nesponuji urcity vzor (napr. vuci reg. vyr. apod.)
*/
class DataConstraint extends \Usluz\Core\Error\Data\Data {
	static protected $lcode = 3010;
}