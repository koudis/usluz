<?php
/**
 * @package Usluz\\Usluz\Core\Error\Data
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Data;



/**
 * InstanceOf error
*/
class DataINIConfig extends \Usluz\Core\Error\Data\Data {
	static protected $lcode = 3020;
}