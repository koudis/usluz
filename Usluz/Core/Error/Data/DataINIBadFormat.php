<?php
/**
 * @package Usluz\\Usluz\Core\Error\Data
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Data;



/**
 * Chyba, pokud ja konfiguracni soubor ve spatnem formatu (nelze ho validne "rozparsovat").
*/
class DataINIBadFormat extends \Usluz\Core\Error\Data\Data {
	static protected $lcode = 3040;
}