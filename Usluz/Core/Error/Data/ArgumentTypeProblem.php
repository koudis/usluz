<?php
/**
 * @package Usluz\\Usluz\Core\Error\Data
 * @author Ignum 
*/

namespace Usluz\Core\Error\Data;


/**
 *
*/
class ArgumentTypeProblem extends Data {
	static protected $lcode = 3050;
}
