<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * BadRequestMethod...
*/
class InvalidRequestData extends \Usluz\Core\Error\Comm\Comm {
	static protected $lcode = 2050;
}
