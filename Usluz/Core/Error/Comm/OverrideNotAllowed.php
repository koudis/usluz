<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * OverrieNotAllowed :).
*/
class OverrideNotAllowed extends \Usluz\Core\Error\Comm\Comm {
	static protected $lcode = 2040;
}
