<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * Pokud zjistime, ze pozadovany trigger neexistuje,
 * vyhodime vyjimku dole
*/
class TriggerNotExist extends \Usluz\Core\Error\Comm\Comm {
	static protected $code = 2030;
}
