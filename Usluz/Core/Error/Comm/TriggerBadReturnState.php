<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * Chyba, pokud 'execute' funkce nejakeho triggeru vrati
 * nevalidni stav.
*/
class TriggerBadReturnState extends \Usluz\Core\Error\Comm\Comm {
	static protected $code = 2020;
}