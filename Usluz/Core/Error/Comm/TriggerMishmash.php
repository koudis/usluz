<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * Dojdeli k nekonzistenci vetsinou behem inicializece
*/
class TriggerMishmash extends \Usluz\Core\Error\Comm\Comm {
	static protected $lcode = 2070;
}
