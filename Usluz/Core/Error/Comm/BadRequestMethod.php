<?php
/**
 * @package Usluz\\Usluz\Core\Error\Comm
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\Comm;



/**
 * InstanceOf error
*/
class BadRequestMethod extends \Usluz\Core\Error\Comm\Comm {
	static protected $code = 2050;
}
