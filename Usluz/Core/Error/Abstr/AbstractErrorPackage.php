<?php
/**
 * @package Usluz\\Usluz\Core\Error\Abstr
 * @author Ignum 
*/

namespace Usluz\Core\Error\Abstr;



/**
 *
*/
abstract class AbstractErrorPackage extends \Exception {
	/**
	 * Chybovy celociselny kod chyby :)
	 * @var integer
	*/
	static protected $lcode = 0;



	/**
	 * Vrati chybovou hlasku dane chyby
	 * @return string
	*/
	public function get_message() {
		return $this->message;
	}

	/**
	 * Vrati chybovy kod dane chyby
	 * @return int
	*/
	static public function get_lcode() {
		return static::$lcode;
	}
}