<?php
/**
 * @package Usluz\\Usluz\Core\Error\EClass
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\EClass;



/**
 * Class not exists
*/
class NotExists extends \Usluz\Core\Error\EClass\EClass {
	static protected $lcode = 1030;
}