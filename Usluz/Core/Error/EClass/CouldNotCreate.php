<?php
/**
 * @package Usluz\\Usluz\Core\Error\EClass
 * @author Ignum 
*/

namespace Usluz\Core\Error\EClass;



/**
 *
*/
class CouldNotCreate extends \Usluz\Core\Error\Abstr\AbstractErrorPackage {
	static protected $lcode = 1040;
}