<?php
/**
 * @package Usluz\\Usluz\Core\Error\EClass
 * @author Ignum 
 *
*/

namespace Usluz\Core\Error\EClass;



/**
 * InstanceOf error
*/
class EInstanceOf extends \Usluz\Core\Error\EClass\EClass {
	static protected $lcode = 1020;
}