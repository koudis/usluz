<?php
/**
 * @package Usluz\\Usluz\Core\Process\Help
 * @author Ignum
*/

namespace Usluz\Core\Process\Help;
use \Usluz\Core\Error;
use \Usluz\Core\Other;



/**
 * Trida vytvarejici  
*/
class Creator {
	/**
	 * Create an instance of Process class
	 * @param string $method
	 * @param string $submethod
	 * @param string $namespace
	 * @return \Usluz\Core\Process\Abstr\AbstractProcessMethod
	*/
	static public function &create_process(&$conf, $method, $submethod) {
		$namespace = ($_t = $conf->get_value('namespace')) ? $_t : '';
		$class_name = $namespace . '\\' . $method . '\\' . $submethod;

		$user_conf = $conf->create_conf_from('user');
		$process = \Usluz\Core\Other\Help\Autoload::_new($class_name, $user_conf);
		if(!$process)
			Throw new Error\EClass\NotExists('Process class '. $class_name . ' not exist.');

		$process->set_conf($user_conf);
		return $process;
	}
}
