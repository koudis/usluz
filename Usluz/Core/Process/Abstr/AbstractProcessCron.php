<?php
/**
 * @package Usluz\Usluz\Core\Process\Abstr
 * @author Ignum
*/

namespace Usluz\Core\Process\Abstr;
use \Usluz\Core;



/**
 * Abstract process class with cron support
*/
abstract class AbstractProcessCron extends \Usluz\Core\Process\Abstr\AbstractProcessMethod {

	/**
	 * @see \Usluz\Core\GlobalCron
	 * @var array
	*/
	private $crons = [];

	/**
	 * @param int 
	 * @param callable
	 * @return boolean
	*/
	public function register_cron($how_often, callable $func) {
		$_crons = Core\GlobalCron::$crons; $_dir = Core\GlobalCron::$dir;
		Core\GlobalCron::$crons = $this->crons; 
		Core\GlobalCron::$dir   = "process-cron";

		Core\GlobalCron::add($how_often, $func);

		$this->crons = Core\GlobalCron::$crons;
		Core\GlobalCron::$crons = $_crons;
		Core\GlobalCron::$dir   = &$_dir;

		return true;
	}

	public function process() {
		$this->process_cron();
		return parent::process();
	}

	/**
	 * @return boolean
	*/
	protected function process_cron() {
		if(!$this->crons)
			return true;

		$_crons = Core\GlobalCron::$crons; $_dir = Core\GlobalCron::$dir;
		Core\GlobalCron::$crons = $this->crons; 
		Core\GlobalCron::$dir   = "process-cron";

		Core\GlobalCron::process();

		Core\GlobalCron::$crons = &$_crons;
		Core\GlobalCron::$dir   = &$_dir;

		return true;
	}
}

Core\GlobalDir::add("process-cron", 0770, true);
