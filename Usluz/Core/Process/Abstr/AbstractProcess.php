<?php
/**
 * @package Usluz\\Usluz\Core\Process\Abstr
 * @author Ignum
*/

namespace Usluz\Core\Process\Abstr;



/**
 * Abstract process class...
*/
abstract class AbstractProcess extends \Usluz\Core\Other\Conf\AbstractConfigurable {
	/**
	 * Autentizace
	 * @var \Usluz\Core\Auth\Auth
	*/
	protected $auth = null;

	/**
	 * Komunikacni trida
	 * @var \Usluz\Core\Comm\Comm
	*/
	protected $comm = null;



	/**
	 * @param \Usluz\Core\Auth\Auth $a
	 * @return boolean
	*/
	public function inject_auth(\Usluz\Core\Auth\Auth $a) {
		$this->auth = $a;
		return true;
	}

	/**
	 * @param \Usluz\Core\Comm\Comm $c
	 * @return boolean
	*/
	public function inject_comm(\Usluz\Core\Comm\Abstr\AbstractComm $c) {
		$this->comm = $c;
		return true;
	}
}
