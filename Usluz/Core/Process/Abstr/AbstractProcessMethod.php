<?php
/**
 * @package Usluz\\Usluz\Core\Process\Abstr
 * @author Ignum
*/

namespace Usluz\Core\Process\Abstr;



/**
 * Abstract class for ProcessInstancess
*/
abstract class AbstractProcessMethod extends \Usluz\Core\Process\Abstr\AbstractProcess {
	/**
	 * This function is called by Usluz
	 * @return boolean
	*/
	public function process() {
		return $this->prepare();
	}

	/**
	 * Function implemented by user
	 * Prepare result returned by result() function
	 * @return boolean
	*/
	abstract protected function prepare();

	/**
	 * Function Implemented by user
	 * Return result of prepare function
	 * @return mixed
	*/
	abstract public function result();
}
