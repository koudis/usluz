<?php
/*
 * @author jan.kubalek
 * @package \Usluz\Core
*/

namespace Usluz\Core;
use \Usluz\Core\Other;
use \Usluz\Core\Error;



class GlobalCron {
	/**
	 * [
	 *     <how_often> => [
	 *         0 => <func>,
	 *         1 => <func>,
	 *         2 => <func>,
	 *         ...
	 *     ]
	 * ]
	 * @var array
	*/
	static public $crons = [];

	/**
	 * @var string
	*/
	static public $dir = "global-cron";



	/**
	 * Add func into the cron list
	 * @param callable
	 * @param int Interval (in seconds)
	 * @return boolean
	*/
	static public function add($how_often, callable $func) {
		$_tmp = 1;
		if(!Other\TimeLock::exists(self::$dir . "/" . $how_often))
			$_tmp = Other\TimeLock::lock(self::$dir . "/" . $how_often, $how_often);
		if(!$_tmp)
			throw new Error\Data\Data("Cannot create lock '$how_often'!");

		self::$crons[$how_often][] = $func;

		return true;
	}

	/**
	 * Process all registered crons
	 * @param  boolean
	 * @param  int
	 * @return boolean
	*/
	static public function process($noy = 1) {
		if($noy && Other\Probablity::check_with_probablity())
			return true;
		foreach(self::$crons as $how_often => &$val) {
			if(!Other\TimeLock::check(self::$dir . "/" . $how_often)) {
				Other\TimeLock::lock(self::$dir . "/" . $how_often, $how_often, true);
				foreach($val as &$func)
					$func();
			}
		}
	}
}

GlobalDir::add("global-cron", 0770, true);
