<?php
/**
 * @author jan.kubalek
*/

namespace Usluz\Core;
use \Usluz\Core\Error;



/**
 * Globalni cache pro konfiguracni soubory atd.
*/
class GlobalCache extends Other\Cache\Cache {
	/**
	 * Check if cache for $key exist. If not, return NULL, else return $key id
	 * @param  int
	 * @param  mixed
	 * @return array
	*/
	static public function &key($key, $max_age) {
		$_tmp = NULL;
		if(is_string($key))
			return parent::genere_key($key, 'globalcache', $max_age);

		throw new Error\Data\ArgumentTypeProblem("Key must be a string");
	}
}

GlobalDir::add('globalcache', 0770, true);
