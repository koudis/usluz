<?php
/**
 * @package Usluz\\Usluz\Core\Workflow
 * @author Ignum 
*/

namespace Usluz\Core\Workflow;
use \Usluz\Core\Error;
use \Usluz\Core\Other;
use \Usluz\Core\Other\Help\NamespaceTools;



/**
 * @TODO: umoznit uzivatelem specifikovat "rozhrani"
 * k 'storage'
*/
class WorkflowConf extends \Usluz\Core\Other\Conf\Conf {
	/**
	 * Klice, ktere jsou vyzadovane z prichozicho requestu
	 * @var array
	*/
	protected $keys = array();

	/**
	 * Klice pro identifikaci 'auth_type', 'method_type' apod.
	 *  0 - [ auth_type, required ]
	 *  1 - [ method_type, required ]
	 *  2 - [ submethod_type, required ]
	 * @var array
	*/
	protected $interface = array(
		0 => [ 'auth_type',      0 ],
		1 => [ 'method_type',    1 ],
		2 => [ 'submethod_type', 1 ]
	);

	/**
	 * Vyzadovane konfiguracni "klice". (pokud se seznam rozroste, vytahnout prosim mimo tridu :))
	 * @var array
	*/
	static private $REQUIRED_CONF = array(
		'process' => array(
			'confdir' => array(
				'get' => 1
			),
		),
		'auth' => array(
			'global' => array(
				'default_auth' => 1,
			),
		),
		'comm' => array(
			'response' => array(
				'http' => array(
					'format' => 1
				),
			),
		),
	);



	/**
	 * @param \Usluz\Core\Other\Conf\Conf|array $c
	 * @param array $interface
	*/
	public function __construct($c, array $interface = array()) {
		parent::__construct($c, self::$REQUIRED_CONF);
		$this->validate();

		if($interface) $this->interface = $interface;
		$this->map_keys();

		$_tmpc = &$this->load_global_conf($method);
		$this->splice($_tmpc->get_values());
	}



	/**
	 * Nacte konfiguraci pro process tridu
	 * @return boolean
	*/
	public function conf_for_request(\Usluz\Core\Storage\Storage $s) {
		$_ts = new \Usluz\Core\Storage\ConstraintStorage($s->get_values());
		$_ts->set_constraints($this->keys);
		
		if(!$_ts->check())
			throw new  Error\Comm\InvalidRequestData('One of these keys missing {' . join(', ', array_keys($this->keys)) . '}');

		unset($_ts);

		$_k = &$this->interface;
		$method    = $s->get_value($_k[1][0]);
		$submethod = $s->get_value($_k[2][0]);

		$_tmp = &$this->load_request_conf($method, $submethod);
		$this->splice(array(
			'process' => array(
				'user' => $_tmp->get_values()
			)
		));

		return true;
	}

	/**
	 *
	*/
	private function &load_request_conf(&$method, &$submethod) {
		$process = $this->create_conf_from('process');
		$ns_path = NamespaceTools::namespace_to_path($process->get_config('namespace'));
		$f_ext   = \Usluz\Core\GlobalConf::get('CONF_FILE_EXT');
		$_pbd    = \Usluz\Core\GlobalConf::get('PROCESS_BASE_DIR');
		$path    = $_pbd . $ns_path . $process->get_config('confdir', strtolower($method)) . '/' . strtolower($submethod) . $f_ext;

		return new \Usluz\Core\Other\Conf\Conf($path);
	}

	/**
	 * Nacte globalni konfigurak pro daou sluzbu
	*/
	private function &load_global_conf(&$method) {
		$process = $this->create_conf_from('process');
		$_pbd    = \Usluz\Core\GlobalConf::get('PROCESS_BASE_DIR');
		$f_ext   = \Usluz\Core\GlobalConf::get('CONF_FILE_EXT');

		$ns_path = NamespaceTools::namespace_to_path($process->get_config('namespace'));
		$path    =   $_pbd . '/' . $ns_path . '/' . $process->get_config('globalconf') . $f_ext;

		return new \Usluz\Core\Other\Conf\Conf($path);
	}

	/**
	 * Vytvori z $interface pole pro 'set_constraints'...
	 * @return boolean
	*/
	private function map_keys() {
		foreach($this->interface as $k => $v) {
			if($v[1])
				$this->keys[$v[0]] = 1;
		}
		return true;
	}

	/**
	 * Nevim, proc to tady je, takze to zde necham.
	 * Treba si casem vzpomenu :D
	*/
	private function include_requested_files() {
	}
}
