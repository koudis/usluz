<?php
/**
 * @package Usluz\\Usluz\Core\Workflow
 * @author Ignum 
*/

namespace Usluz\Core\Workflow\Abstr;
use \Usluz\Core\Error;
use \Usluz\Core\Other;
use \Usluz\Core\Other\Conf\FileParser;
use \Usluz\Core\Workflow\WorkflowConf;



/**
 * AbstractWorkflow ;)
*/
abstract class AbstractWorkflow {
	/**
	 * @var \Usluz\Core\Workflow\WorkflowConf
	*/
	protected $conf;

	/**
	 * @var \Usluz\Core\Comm\Comm
	*/
	protected $comm;

	/**
	 * @var \Usluz\Core\Auth\Auth
	*/
	protected $auth;

	/**
	 * @var \Usluz\Core\Process\Abstr\AbstractProcessMethod
	*/
	protected $process;

	/**
	 * Arguments passed to Comm class...
	 * @var array
	*/
	protected $args;

	/**
	 * Required attributes
	 * @var array
	*/
	static protected $REQUIRED = array(
		'method_type'    => '/.+/',
		'submethod_type' => '/.+/'
	);

	/**
	 * Jake vsechny klice se musi vyskytovat v Conf 
	 * @var array
	*/
	static protected $REQUIRED_CONF = array(
		'comm'    => 1,
		'auth'    => 1,
		'process' => array(
			'user' => 1   // sem se nacte konfigurace pro dany "process"
		)
	);



	public function __construct(\Usluz\Core\Other\Conf\Conf &$conf = NULL) {
		try {
			if($conf) {
				$this->conf = new WorkflowConf($conf);
			} else {
				$_cdir      = \Usluz\Core\GlobalConf::get('CONF_DIR');
				$_conf_file = $_cdir  . \Usluz\Core\GlobalConf::get('MAIN_CONF_FILE_NAME');
				$this->conf = new WorkflowConf($_conf_file);

				$comm_conf  = $this->conf->create_conf_from('comm');
				$this->comm = &static::create_comm($comm_conf);
				$this->comm->add_request_constraints(static::$REQUIRED);
			}
		} catch(Error\Abstr\AbstractErrorPackage $e) {
			static::handle_critic_error($e);
			exit(1);
		}
	}



	/**
	 * Return reference to the "next" arg passed to comm class...
	 * @return reference
	*/
	protected function &get_next_arg() {
		$this->args[] = NULL;
		return $this->args[count($this->args) - 1];
	}



	/**
	 * Spust vsechno co muzes a ocekavej prichod Mojcka.
	 * @param  various
	 * @return boolean
	*/
	public function run(...$args) {
		try {
			$len = count($args);
			if($len > count($this->args))
				throw new Error\Data\DataConstraint("Too many arguments passed to run function");
			for($i = 0; $i < $len; $i++)
				$this->args[$i] = $args[$i];
			return $this->execute();
		} catch(\Exception $e) {
			static::handle_critic_error($e);
			exit(1);
		}
	}


	/**
	 * Pripravi prostredi pro RUN
	 * @param \Usluz\Core\Other\Conf\Conf $ini_file
	*/
	protected function prepare(...$args) {
		$comm_conf  = $this->conf->create_conf_from('comm');
		$this->comm = &static::create_comm($comm_conf, ...$args);
		$this->comm->add_request_constraints(static::$REQUIRED);
	}

	/**
	 * Spust samotny process
	 * @return boolean
	*/
	abstract protected function execute();

	/**
	 * Funkce, ktera je zavolana, nestaneli kriticky error (nejsme schopni napr. ani odeslat zpravu o chybe)
	*/
	static protected function handle_critic_error(\Exception &$e) {}

	/**
	 * Vytvori komunikacni tridu
	 * @return \Usluz\Core\Comm\Comm Instance tridy \Comm\Comm
	*/
	protected function &create_comm(\Usluz\Core\Other\Conf\Conf &$conf) {}

	/**
	 * Vytvori "auth" tridu
	 * @param \Usluz\Core\Comm\Comm &$comm
	 * @return \Usluz\Core\Auth\Auth Instance tridy \Auth\Auth
	*/
	static protected function &create_auth(\Usluz\Core\Other\Conf\Conf &$conf, \Usluz\Core\Comm\Abstr\AbstractComm &$comm, $global_auth = '') {
		$auth = new \Usluz\Core\Auth\Auth($conf, $global_auth);
		$auth->inject_comm($comm);
		return $auth;
	}

	/**
	 * Vytvori "process" tridu
	 * @param \Usluz\Core\Comm\Comm &$comm
	 * @param \Usluz\Core\Auth\Auth &$auth
	 * @return \Usluz\Core\Process\Process Instance tridy \Usluz\Core\Process\Process
	*/
	static protected function &create_process(\Usluz\Core\Other\Conf\Conf &$conf, \Usluz\Core\Comm\Abstr\AbstractComm &$comm, \Usluz\Core\Auth\Auth &$auth, $method, $submethod) {
		$process = &\Usluz\Core\Process\Help\Creator::create_process($conf, $method, $submethod);
		$process->inject_comm($comm);
		$process->inject_auth($auth);
		return $process;
	}
}
