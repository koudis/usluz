<?php
/**
 * @package Usluz\Core\Workflow
 * @author Ignum
*/

namespace Usluz\Core\Workflow;
use \Usluz\Core\Error;
use \Usluz\Core\Other;

abstract class Workflow extends Abstr\AbstractWorkflow {
	public function __construct(\Usluz\Core\Other\Conf\Conf &$conf = NULL) {
		parent::__construct($conf);
	}



	/**
	 * Spust vsechno co muzes a ocekavej prichod Mojcka.
	 * @return boolean
	*/
	protected function execute() {
		try {
			$com_r = $this->comm->process_request();
			$req_storage = $this->comm->get_request_storage();

			$this->conf->conf_for_request($req_storage);
			$this->conf->add_constraints(self::$REQUIRED_CONF);
			$this->conf->validate();

			$auth_conf = $this->conf->create_conf_from('auth');
			$this->auth = &static::create_auth($auth_conf, $this->comm, $req_storage->get_value('auth_type'));
			$this->auth->validate();

			$result = null;
			if($com_r) {
				$process_conf   = $this->conf->create_conf_from('process');
				$method_type    = $req_storage->get_value('method_type');
				$submethod_type = $req_storage->get_value('submethod_type');
				$this->process  = &static::create_process($process_conf, $this->comm, $this->auth, $method_type, $submethod_type);
				$this->process->process();
				$result = $this->process->result();
				$this->comm->set_response_storage($result);
			}

			return $this->comm->process_response();

		} catch(Error\Abstr\AbstractErrorPackage $e) {
			$s = new \Usluz\Core\Storage\Storage();
			$s->set_value('code', $e->get_lcode());
			$s->set_value('error_class', get_class($e));
			$s->set_value('error_message', $e->get_message());
			try {
				$er = $this->comm->get_error_response();
				$er->set_error($e);
				$er->set_storage($s); 
				echo $er->process();
			} catch(\Exception $e) {
				static::handle_critic_error($e);
				exit(2);
			}

			return false;
		} catch(\Exception $e) {
		// tato sekce kvuli PROCESS tride (nevime, co uzivatel pouziva za dalsi knihovny)
			$s = new \Usluz\Core\Storage\Storage();
			$s->set_value('code', -1);
			$s->set_value('error_class', get_class($e));
			$s->set_value('error_message', $e->getMessage());
			try {
				$er = $this->comm->get_error_response();
				$er->set_extern_error($e);
				$er->set_storage($s); 
				echo $er->process();
			} catch(\Exception $e) {
				static::handle_critic_error($e);
				exit(3);
			}

			return false;
		}

		return true;
	}
}
