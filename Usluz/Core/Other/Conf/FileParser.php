<?php
/**
 * @package Usluz\\Usluz\Core\Other\Conf
 * @author ignum
*/

namespace Usluz\Core\Other\Conf;
use \Usluz\Core\Error;
use \Usluz\Core;
use Symfony\Component\Yaml;



class FileParserCache extends Core\GlobalCache {
	static protected function genere_key_value(&$key) {
		return preg_replace('#[\\\\/\.]+#', '_', $key);
	}
}

class FileParser {
	const FILE_EXTENSION = '.yaml';

	static public function parse($file) {
		$key = &FileParserCache::key($file, 64000);	
		if(FileParserCache::check($key))
			return FileParserCache::load($key);

		$val = Yaml\Yaml::parse(file_get_contents($file));
		FileParserCache::write($key, $val);
		return $val;

	//	return yaml_parse_file($file);
	}
}
