<?php
/**
 * @package Usluz\\Usluz\Core\Other\Conf
 * @author ignum
*/

namespace Usluz\Core\Other\Conf;
use \Usluz\Core\Error;



/**
 * Trida pro "sosani" konfigurece
*/
class Conf extends \Usluz\Core\Storage\ConstraintStorage implements \ArrayAccess {
	/**
	 * Jmeno souboru s konfiguraci...
	 * @var string
	*/
	private $file_name = '';


	/**
	 * @param string|array $ini_file
	*/
	public function __construct($file = array(), $constraint = array()) {
		parent::__construct();
		if(is_string($file)) {
			$this->file_name = $file;
			if(file_exists($file)) {
				$_t = FileParser::parse($file);
				if((!$_t) || (!is_array($_t))) { throw new Error\File\BadINI('BadINI format. file ' . $file ); }
				$this->set_values($_t);

			} else {
				throw new Error\File\FileNotExist('Required INI file not exist - ' . $file);
			}
		} else if(is_array($file)) {
			$this->file_name = null;
			$this->set_values($file);
		} else {
			throw new Error\Data\DataINIConfig('Ouuu, argument is not array - ' . print_r($file, true));			
		}

		$this->set_constraints($constraint);
	}



	/**
	 * Implements 
	 * @param string key
	 * @return mixed
	*/
	public function offsetGet($key) {
		return $this->get_config($key);
	}
    public function offsetExists($key) {
		$conf = &$this->get_values();
        return isset($conf[$key]);
    }

    public function offsetUnset($offset) {
		throw new Error\Data\Data("Cannot use UNSET implementation of array interface!");
    }
    public function offsetSet($offset, $value) {
		throw new Error\Data\Data("Cannot use SET implementation of array interface!");
	}



	/**
	 * @see \Usluz\Core\Storage\SpliceStorage::splice_merge_function
	*/
	protected function splice_merge_function(&$a1, &$a2) {
		return array_replace_recursive($a1, $a2);
	}



	/**
	 * Nastavi sekci 
	 * @param string $section
	 * @param mixed $c
	 * @return mixed
	*/
	protected function set_section($section, $c) {
		$this->set_value($section, $c);
		return $c;
	}

	/**
	 * Vrati konfiguraci podle KLICEa a SEKCE
	 * @param string $section
	 * @param string $key
	 * @return mixed
	*/
	public function get_config($section, $key = '') {
		$_tmp = &$this->get_values();
		if($this->exist($section) && (!$key)) {
			return $_tmp[$section];
		} else if($this->exist($section, $key) && $key) {
			return $_tmp[$section][$key];
		} else {
			throw new Error\Data\DataINIConfig("Conf: Waaw, SECTION '$section' or KEY '$key' in section '$section' was not set. File " . print_r($this->file_name, true));
		}
	}

	/**
	 * Vrati celou sekci...
	 * @param string $section
	 * @return mixed
	*/
	public function get_section($section) {
		$_tmp = &$this->get_values();
		if(isset($_tmp[$section])) {
			return $_tmp[$section];
		} else {
			throw new Error\Data\DataINIConfig("Conf: Waaw, SECTION '$section' was not set. File " . print_r($this->file_name, true));
		}
	}

	/**
	 * Zjisti, jestli dany "config" existuje
	 * @param string $section
	 * @param string $key
	 * @return mixed
	*/
	public function exist($section, $key = '') {
		$_tmp = &$this->get_values();
		if($key) return (isset($_tmp[$section]) && isset($_tmp[$section][$key]));
		else     return (isset($_tmp[$section]));
	}

	/**
	 * Vrati jmeno souboru z nehoz byla nactena konfigurace
	 * @return string 
	*/
	public function get_file_name() {
		return $this->$file_name;
	}

	/**
	 * Vytvori ze sekce Conf tridu. Pokud neexistuje, vrati NULL
	 * @param  string $section
	 * @return NULL|\Usluz\Core\Other\Help\Conf
	*/
	public function &create_conf_from($section, $key = '') {
		if ($this->exist($section, $key)) {
			return new Conf($this->get_config($section, $key));
		} else {
			throw new Error\Data\DataINIConfig("Conf: Waaw, SECTION '$section' was not set. File " . print_r($this->file_name, true));
		}
	}

	/**
	 *
	*/
	public function validate() {		
		$_tmp = parent::check();
		if(!$_tmp) {
			throw new Error\Data\DataINIConstraint('ConfConstraint: missing required attributes');
		}

		return true;
	}
}
