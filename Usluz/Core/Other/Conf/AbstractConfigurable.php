<?php
/**
 * @author Ignum
 * @package Usluz\\Usluz\Core\Other\Conf
*/

namespace Usluz\Core\Other\Conf;
use \Usluz\Core\Error;



abstract class AbstractConfigurable extends \Usluz\Core\Other\Object\ObjectCastProperty {
	/**
	 * konfigurace dane tridy
	 * @var \Usluz\Core\Other\Conf\Conf
	*/
	private $conf = null;



	public function __construct(\Usluz\Core\Other\Conf\Conf $c = null) {
		if(!$c) {
			$this->conf = new Conf();
		} else {
			$this->conf = $c;
		}
	}



	/**
	 * Nastavbi konfiguraci pro danou tridu
	 * @param \Usluz\Core\Other\Conf\Conf $c
	 * @return \Usluz\Core\Other\Conf\Conf $c
	*/
	public function set_conf($c) {
		$this->conf = $c;
		return $c;
	}

	/**
	 * @return \Usluz\Core\Other\Conf\Conf
	*/
	public function &get_conf() {
		return $this->conf;
	}
}