<?php
/**
 * @author jan.kubalek
 *
*/

namespace Usluz\Core\Other;
use \Usluz\Core;
use \Usluz\Core\Error;



/**
 * Class for handle timelock...
*/
class TimeLock {
	/**
	 * @param  string lock name
	 * @param  INT
	 * @return boolean
	*/
	static public function lock($name, $offset, $override = 0) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(!$override && file_exists($path))
			throw new Error\Data\DataConstraint("Name $name already exists");
		if($offset <= 0)
			throw new Error\Data\DataConstraint("Offset is le zero!");
		return touch($path, time() + $offset);
	}

	/**
	 * @param String Lock name
	 * @return INT unlink return value
	 *
	*/
	static public function unlock($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(file_exists($path))
			return unlink($path);
	}

	/**
	 * @param $name
	*/
	static public function check($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(filemtime($path) < time())
			return false;
		return true;
	}

	/**
	 * @param string lock name
	 *
	*/
	static public function exists($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		return file_exists($path);
	}
}
