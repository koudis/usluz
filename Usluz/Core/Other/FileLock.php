<?php
/**
 * @author jan.kubalek
 *
*/

namespace Usluz\Core\Other;
use \Usluz\Core\Error;



/**
 * Class for handle filelock...
*/
class FileLock {
	/**
	 * @param  INT valid file descriptor
	 * @param  INT file mode
	 * @return boolean
	*/
	static public function lock($fd, $mode) {
		return flock($fd, $mode);
	}

	/**
	 * @param  INT valid file descriptor
	 * @return boolean
	*/
	static public function unlock($fd) {
		return flock($fd, LOCK_UN);
	}

	/**
	 * If file is locked, then wait 
	 * @param  INT valid file descriptor
	 * @param  INT file mode
	 * @return boolean
	*/
	static public function lock_or_wait($fd, $mode) {
		$lo = 0;
		$_tmp = flock($fd, $mode | LOCK_NB, $lo);
		if(!$_tmp && $lo) {
			$k = 0;
			while(1) {
				$k += 1;
				usleep(2000 + $k);
				$_tmp = flock($fd, $mode | LOCK_NB);
				if(!$_tmp && $k < 150)
					continue;
				if(!$_tmp && $k >= 150)
					throw new Error\File\FileUnexpectedError("Cannot lock file!");
				return true;
			}
		}
		if(!$_tmp && !$lo)
			throw new Error\File\FileUnexpectedError("Cannot lock file!");

		return true;
	}
}
