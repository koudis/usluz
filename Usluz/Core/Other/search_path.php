<?php

namespace Usluz\Core\Other\SearchPath;



function get_local_directories() {
	return glob('*', GLOB_ONLYDIR);
}

function &get_subdirectories($dir, &$list = null) {
	$_t = getcwd();
	chdir($dir);

	if(is_null($list)) { $list = array(); }

	$_d = get_local_directories();
	$l = end($list) ? end($list) . DIRECTORY_SEPARATOR : $dir . DIRECTORY_SEPARATOR;
	foreach($_d as $key => $value) {
		$list[] = $l . $value;
		get_subdirectories($value, $list);
	}

	chdir($_t);
	return $list;
}

function get_subdirectories_by_array($dir) {
	$list = '';
	foreach($dir as $v) {
		$list .= join(PATH_SEPARATOR, get_subdirectories($v)) . PATH_SEPARATOR;
	}

	return $list;
}
