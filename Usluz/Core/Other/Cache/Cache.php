<?php
/**
 *
 *
*/

namespace Usluz\Core\Other\Cache;
use \Usluz\Core\GlobalConf;
use \Usluz\Core;



/**
 * Obecna trida pro cache
 *
*/
class Cache {

	/**
	 * @var int
	*/
	const KEY = 0;

	/**
	 * @var int
	*/
	const PATH = 1;

	/**
	 * @var int
	*/
	const MAX_AGE = 2;



	/**
	 * Vygeneruje klic pro cache
	 * @param mixed  Attribs for key genertor
	 * @param string Directory name for cache
	 * @return array Valid cache keys
	*/
	static public function genere_key(&$salt, $dir, $max_age) {
		if($max_age > Core\GlobalConf::get("MAX_CACHE"))
			throw new Error\Data\DataINIConstraint("Max age ");

		$key = static::genere_key_value($salt);
		return [
			$key,
			static::genere_path($key, $dir),
			$max_age
		];
	}	
	
	/**
	 * Zkontroluje, zdali je cache validni
	 * @param  string cache key
	 * @return boolean
	*/
	static public function check(array &$key) {
		$path = &$key[self::PATH];

		if(!file_exists($path))
			return false;

		if(filemtime($path) < time()) {
			unlink($path);
			return false;
		}

		return true;
	}

	/**
	 * Zapise v serializovane podobe hodnotu $value
	 * @param array Valid cache key
	 * @param mixed Valid value
	 * @return boolean
	*/
	static public function write(array &$key, $value) {
		$path = &$key[self::PATH];
		$fd   = Lib::open_file($path);
		if(!$fd)
			throw new \Exception("Cannot open file $path");

		$_tmp = Lib::write($fd, $value);
		Lib::close_file($fd);
		if(!$_tmp)
			throw new \Exception("Cannot save faile...");

		$_tmp = touch($path, time() + $key[self::MAX_AGE]);
		if(!$_tmp)
			throw new \Exception("Cnnot 'touch' file");

		return true;
	}

	/**
	 * Nacte obsah cache
	 * @param array
	 * @return mixed
	*/
	static public function &load(array &$key) {
		$fd       = Lib::open_file($key[self::PATH]);
		$response = Lib::read($fd);
		Lib::close_file($fd);
		return $response;
	}



	/**
	 * Vygeneruje cestu k souboru
	 * @param array $key
	 * @return string
	*/
	static protected function genere_path(&$key, &$dir) {
		return GlobalConf::get('TEMPORARY_DIR') . DIRECTORY_SEPARATOR . $dir .
			DIRECTORY_SEPARATOR . $key;
	}

	/**
	 *
	 *
	*/
	static protected function genere_key_value(&$salt) {
		return Lib::genere_key($salt);
	}
}
