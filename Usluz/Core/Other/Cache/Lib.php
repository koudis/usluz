<?php
/**
 * @author  ignum
 * @package Usluz\Triggers\Cache\Lib
*/

namespace Usluz\Core\Other\Cache;
use \Usluz\Core\Other;



/**
 * Mala knihovnicka funkci pro Cache
*/
class Lib {
	/**
	 * Vygeneruje klic, pod kterym bude dany objek "zacachovan"
	 * @param array $request_values
	 * @return string
	*/
	static public function genere_key($request_values) {
		ksort($request_values);
		$keys   = array_keys($request_values);
		$values = array_values($request_values); 
		return md5(join('', $keys) . join('', $values));
	}

	/**
	 * Otevre soubor pro cteni a zapis :).
	 * @param  string $path
	 * @return file_descriptor
	*/
	static public function open_file($path) {
		return fopen($path, 'a+');
	}

	/**
	 * Zavre file_desriptor
	 * @return
	*/
	static public function close_file($file_descriptor) {
		return fclose($file_descriptor);
	}

	/**
	 * Zapise objekt Storage bezpecne do souboru
	 * @param  INT file_descriptor
	 * @param  mixed
	 * @return Chybovy kod...
	*/
	static public function write($file_descriptor, &$data) {
		$_tmp = Other\FileLock::lock_or_wait($file_descriptor, LOCK_EX);
		if(!$_tmp)
			return $_tmp;

		$_tmp = fwrite($file_descriptor, serialize($data));
		if(!$_tmp)
			return $_tmp;

		$_tmp = Other\FileLock::unlock($file_descriptor);
		if(!$_tmp)
			return $_tmp;

		return true;
	}

	/**
	 * Nacte objekt Storage z souboru, na ktery ukazuje file_descriptor
	 * (nemeni jeho "pozici" v soubotu pred zapocetim cteni)
	 * @param int file_descriptor
	 * @return \Usluz\Core\Storage\Storage|false
	*/
	static public function read($file_descriptor) {
		$_tmp = '';
		Other\FileLock::lock($file_descriptor);
		while(!feof($file_descriptor)) {
			$data = fread($file_descriptor, 1024);
			$_tmp .= $data;
		}

		return unserialize($_tmp);
	}
}
