<?php
/**
 * @package Usluz\\Usluz\Core\Other\Object
 * @author Ignum 
*/

namespace Usluz\Core\Other\Object;



/**
 * 
*/
class ObjectCopy extends ObjectCastVirtual {
	public function _create_child($child_name) {
		return $this->cast($child_name, ObjectCastProperty::FORCE);
	}
}