<?php

use \Usluz\Core\Error;



$ABS = realpath(dirname(__FILE__));
require_once $ABS . DIRECTORY_SEPARATOR . '..'  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'autoload.php';


class a extends Core\Other\Object\ObjectCastProperty {
	public $a  = 'a-a';
	protected $e = 'a-e';
	private $c = 'a-c';

	public function __construct() {
		$this->c = 'ssss';
	}

	public function aaa() {
		print 'a: ' .$this->a . '<br>c: ' . $this->c . '<br>e: ' . $this->e;
	}

	public function get_c() { return $this->c; }
	public function get_e() { return $this->e; }
}

class b extends a {
	public $a = "b-a";
	protected $e = "b-e";
	private $c = "b-c";

	public function __construct() {
		parent::__construct();
	}

	public function aaa() {
		print $this->b;
	}

	public function get_c() { return $this->c; }
	public function get_e() { return $this->e; }
}

class c extends b {
	public $a = 'c-a';
}



class Test_ObjectCastProperty extends \PHPUnit_Framework_TestCase {

	public function createClass_a() {
		$t = new a();
		return $t;
	}

	public function createClass_b() {
		$t = new b();
		return $t;
	}

	public function createClass_c() {
		$t = new c();
		return $t;
	}

	public function testCast_a_to_b() {
		$a = $this->createClass_a();
		$this->assertInstanceOf('b', $a->cast('b', true));
	}

	/**
	 * @expectedException \Usluz\Core\Error\EClass\EInstanceOf
	*/
	public function testCast_a_einstanceof() {
		$a = $this->createClass_a();
		$a->cast('b');
	}

	/**
	 * @expectedException \Usluz\Core\Error\EClass\NotExists
	*/
	public function testCast_a_notexist() {
		$a = $this->createClass_a();
		$a->cast('bdjkdjskdsk');
	}

	public function testCast_b_to_a() {
		$b = $this->createClass_b();
		$this->assertInstanceOf('a', $b->cast('a'));
	}

	public function testCastProperty_b_to_a() {
		$b = $this->createClass_b();
		$a = $b->cast('a');
		$this->assertEquals('b-a', $a->a);
		$this->assertEquals('b-e', $a->get_e());
		$this->assertEquals('ssss', $a->get_c());
	}

	public function testCastProperty_c_to_a() {
		$c = $this->createClass_c();
		$a = $c->cast('a');
		$this->assertEquals('c-a', $a->a);
		$this->assertEquals('b-e', $a->get_e());
		$this->assertEquals('ssss', $a->get_c());
	}

	public function testCastProperty_c_to_b() {
		$c = $this->createClass_c();
		$a = $c->cast('b');
		$this->assertEquals('c-a', $a->a);
		$this->assertEquals('b-e', $a->get_e());
		$this->assertEquals('b-c', $a->get_c());
	}
}