<?php
/**
 * @package Usluz\\Usluz\Core\Other\Object
 * @author Ignum 
*/

namespace Usluz\Core\Other\Object;
use Usluz\Core\Other\Help\_Storage;
use Usluz\Core\Error;



/**
 * Tato trida moznuje pretypovani objektu
 * vzhledem k promennym
*/
class ObjectCastProperty extends Object {
	/**
	 * Hexadecimalni equivalence prefixu protected atributu tridy
	 * @var string
	*/
	const PROTECTED_SERIALIZE_PREFIX_1 = '00';

	/**
	 * Hexadecimalni equivalence prefixu protected atributu tridy
	 * @var string
	*/
	const PROTECTED_SERIALIZE_PREFIX_2 = '2a';

	/**
	 * Hexadecimalni equivalence prefixu protected atributu tridy
	 * @var string
	*/
	const PROTECTED_SERIALIZE_PREFIX_3 = '00';

	/**
	 * Hexadecimalni equivalence prefixu private atributu tridy
	 * @var string
	*/
	const PRIVATE_SERIALIZE_PREFIX = '00';

	/**
	 * Pouzit pro vynuceni 'force cast' u funkce 'cast'
	 * @var boolean
	*/
	const FORCE = true;



	public function __costruct() {
	}



	/**
	 * Pretypuje objekt na sebe sama ci na libovolneho predka
	 * Serializuje puvodi objekt, zameni jmena objektu, oreze promenne a odserializuje
	 * @param string $to_class jmeno tridy, na kterou ma byt pretypovano
	 * @param boolean $rewrite_cast povoli "tvrde" pretypovani (neohlizi se na typ a neorezava argumenty)
	 * @return \$to_class|null 
	*/
	public function cast($to_class, $rewrite_cast = false) {
		if(class_exists($to_class)) {

			$obj_out = '';
			if(!$rewrite_cast) {
				$out_args = $this->prepare_for_cast($to_class);
				$obj_out = 'O:' . strlen($to_class) . ':"' . $to_class . '":';
				$obj_out .= count($out_args) . ':{' . join('', $out_args) . '}';
			} else {
				$obj_in  = serialize($this);
				$matches = '';
				preg_match('/^O:[[:digit:]]+:[^:]*:(.*)$/', $obj_in, $matches);
			    $obj_out = 'O:' . strlen($to_class) . ':"' . $to_class . '":' . $matches[1];
			}

			if($this instanceof $to_class) {
				$obj_out_inst = unserialize($obj_out);
				return $obj_out_inst;
			} else if($rewrite_cast) {
				$obj_out_inst = unserialize($obj_out);
				return $obj_out_inst;
			} else {
				throw new Error\EClass\EInstanceOf('Cast error ' . get_class($this) .' ---> ' . $to_class);
			}
		}
		else {
			throw new Error\EClass\NotExists('Class ' . $to_class . ' not exists!');
		}
	}	

	/**
	 * Pripravi argumenty pro pretypovani
	 * @param string &$to_class
	 * @return array kazdy prvek pole obsahuje jeden serilizovany arg. z tridy
	*/
	private function prepare_for_cast(&$to_class) {
		$in_args  = new \ReflectionClass($this);
		$out_args = new \ReflectionClass($to_class);

		$in_list1  = array();
		$out_list1 = array();
		$out_args_public = $out_args->getProperties(\ReflectionProperty::IS_PUBLIC);
		if($out_args_public) {
			$in_args_public = $in_args->getProperties(\ReflectionProperty::IS_PUBLIC);
			$in_list1  = self::get_reflection_keys($in_args_public);
			$out_list1 = self::get_reflection_keys($out_args_public);
		}

		$in_list2  = array();
		$out_list2 = array();
		$out_args_protected = $out_args->getProperties(\ReflectionProperty::IS_PROTECTED);
		if($out_args_protected) {
			$in_args_protected = $in_args->getProperties(\ReflectionProperty::IS_PROTECTED);
			$in_list2  = self::get_reflection_keys($in_args_protected);
			$out_list2 = self::get_reflection_keys($out_args_protected);
		}

		$public    = array_intersect_key($in_list1, $out_list1);
		$protected = $in_list2;

		$private = array();
		while($parent = $in_args->getParentClass()) {
			$p = $parent->getProperties(\ReflectionProperty::IS_PRIVATE);
			$private = array_merge($private, $p);
			$in_args = &$parent;
		}
		$private = self::get_reflection_keys($private);

		$public_serialize    = &$this->serialize_args($public, self::_PUBLIC);
		$protected_serialize = &$this->serialize_args($protected, self::_PROTECTED);
		$private_serialize   = &$this->serialize_args($private, self::_PRIVATE);

		return array_merge($protected_serialize, $public_serialize, $private_serialize);
	}

	/**
	 * Vrati pole serializovanych argumentu predanych pomoci $r
	 * @param array &$r
	 * @param string $type
	 * @see ObjectCastProperty::get_replection_keys
	*/
	private function &serialize_args(array &$r, $type) {
		$prefix = '';
		$to_class_args = array();
		foreach($r as $k => $v) {
			$prefix = self::create_prefix($type, $v);
			$_t = 's:' . (mb_strlen($prefix.$k)) . ':"' . $prefix.$k . '";' . \serialize($v->getValue($this));
			array_push($to_class_args, $_t);
		}

		return $to_class_args;
	}

	/**
	 * Podle urovne pristupu urci prefix pro serializaci
	 * @param string $type
	 * @param ReflectionProperty &$property
	*/
	static private function create_prefix($type, &$property = NULL) {
		switch($type) {
			case self::_PRIVATE:
				return pack('H2', self::PRIVATE_SERIALIZE_PREFIX) . $property->class . pack('H2', self::PRIVATE_SERIALIZE_PREFIX);
				break;
			case self::_PROTECTED:
				list($a, $b, $c) = array(self::PROTECTED_SERIALIZE_PREFIX_1, self::PROTECTED_SERIALIZE_PREFIX_2, self::PROTECTED_SERIALIZE_PREFIX_3);
				return pack('H2H2H2', $a, $b, $c);
				break;
			case self::_PUBLIC:
				return '';
				break;
			default:
				throw new Error\Compile\Compile('serialize_args: invalid choose!');
		}
	}

	/**
	 * Premapuje pole array(ReflectionClass) na array(ReflectionProperty->name) = &ReflectionPreperty
	 * @param array &$r
	*/
	static private function &get_reflection_keys(array &$r) {
		_Storage::$list = array();
		array_map(
			function($v) {
				_Storage::$list[$v->name] = &$v;
				_Storage::$list[$v->name]->setAccessible(true);
			},
			$r
		);
		return _Storage::$list;
	}
}
