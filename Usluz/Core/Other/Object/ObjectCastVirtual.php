<?php
/**
 * @package Usluz\\Usluz\Core\Other\Object
 * @author Ignum 
*/

namespace Usluz\Core\Other\Object;



/**
 * Tato trida mela podporovat virtualni funkce na zpusob C++.
 * V dobe implementace vsak neexistoval jednoduchy zpusob, jak tuto
 * funkcnost implementovat.
*/
class ObjectCastVirtual extends ObjectCastProperty {
}