<?php
/**
 * @package Usluz\\Usluz\Core\Other\Object
 * @author ignum
*/

namespace Usluz\Core\Other\Object;



/**
 * Zakladni trida pro vsechny objekty
*/
class Object {
	/**
	 * Konstanta, pomoci ktere je v odvoz. tridach
	 * identifikovano omezeni pristupu k urcitemu objektu
	 * @var
	*/
	const _PRIVATE = 'private';

	/**
	 * Konstanta, pomoci ktere je v odvoz. tridach
	 * identifikovano omezeni pristupu k urcitemu objektu
	 * @var
	*/
	const _PROTECTED = 'protected';

	/**
	 * Konstanta, pomoci ktere je v odvoz. tridach
	 * identifikovano omezeni pristupu k urcitemu objektu
	 * @var
	*/
	const _PUBLIC = 'public';
}

