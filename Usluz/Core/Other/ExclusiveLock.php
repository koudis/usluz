<?php
/**
 * @author jan.kubalek
 *
*/

namespace Usluz\Core\Other;
use \Usluz\Core;
use \Usluz\Core\Error;



/**
 * Class for Exclusive losk ("Mutex")
*/
class ExclusiveLock {
	/**
	 * @param  string lock name
	 * @param  INT
	 * @return boolean
	*/
	static public function lock($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(file_exists($path))
			throw new Error\Data\DataConstraint("Lock with name $name already exists");
		return touch($path);
	}

	/**
	 * @param String Lock name
	 * @return INT unlink return value
	 *
	*/
	static public function unlock($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(file_exists($path))
			return unlink($path);
	}

	/**
	 * @param $name
	*/
	static public function check($name) {
		$path = Core\GlobalConf::get('TEMPORARY_DIR') . '/' . $name;
		if(!file_exists($path))
			return false;
		return true;
	}
}
