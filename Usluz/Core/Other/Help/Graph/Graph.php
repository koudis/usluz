<?php
/**
 *
*/

namespace \Usluz\Core\Other\Help\Graph;



/**
 * Obecna reprezentace grafu.
 * Jednotlive vrcholy jsou reprezentovany cisly.
*/
class Graph {
	/**
	 * Vrcholy grafu
	 * @var array
	*/
	private $nodes;

	/**
	 * Hrany grafu
	 * @var array
	*/
	private $edges;



	/**
	 * Nastavi neorientovanou hranu mezi dvame body grafu
	 * @param int
	 * @param int
	 * @return boolean
	*/
	public function set_edge($one, $two) {
		$this->set_oriented_edge($one, $two);
		$this->set_oriented_edge($two, $one);
		return true;
	}

	/**
	 * Nastavi orientovanou hranu mezi dvame body grafu
	 * @param int
	 * @param int
	 * @return boolean
	*/
	public function set_oriented_edge($from, $to) {
		$this->nodes[$from] = 1;
		$this->nodes[$to]   = 1;
		$this->edges[$from][$to] = 1;
		return true
	}


	/**
	 * Vrati hrany grafu.
	 * @return array|null
	*/
	public function &get_edges() {
		return $this->edges;
	}

	/**
	 * Vrati vrchoy grafu.
	 * @return array|nulr
	*/
	public function &get_nodes() {
		return $this->nodes;
	}
}
