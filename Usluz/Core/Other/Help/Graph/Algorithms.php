<?php
/**
 * @author ignum
*/ 

namespace \Usluz\Core\Other\Help\Graph;



/**
 * Zakladni knihovna algoritmu 
 * (v pripade rozsireni, prosim rozdelte
 * nejak rozumne na vice casti)
*/
class Algorithms {
	/**
	 * Zkobntroluje, zdali jsou v grafy cykly.
	 * @return boolean true - cyklus nalezen, false - cyklus nenalezen
	*/
	static public function check_cycles(Graph &$graph) {
		$nodes = &$graph->get_nodes();
		$edges = &$graph->get_edges();

		$stamp = array(array());
		foreach($edges as $from => $to) {
			$_tmp = false;
			foreach($stamp as $_k) {
				if(isset($stamp[$_k][$from])) {
					if(isset($stamp[$_k][$to])) {
						return true;
					} else {
						$stamp[$_k][$from][$to] = 1;
					}
				} else {
					$_tmp[] = array($from => array($to => 1));
				}
			}
		}

		return false;
	}

	/**
	 * Vyresi zavislosti v grafu (vrati poradi
	 * vrcholu tak, aby byly splneny zavislosti)
	 * @param \Usluz\Core\Other\Help\Graph
	*/
	static public function get_all_paths(Graph &$graph) {
	}

	/**
	 * Vyresi zavislosti v grafu (vrati poradi
	 * vrcholu tak, aby byly splneny zavislosti)
	 * @param \Usluz\Core\Other\Help\Graph
	*/
	static public function solve_dependices(Graph &$graph) {
		if(self::check_cycles($graph))
			return array();

		$nodes = &$graph->get_nodes();
		$edges = &$graph->get_edges();

		foreach($edges as $from => $to)
			unset($nodes[$to]);
	}
}

