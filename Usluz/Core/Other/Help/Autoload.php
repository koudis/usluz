<?php
/**
 * @package Usluz\\Usluz\Core\Other\Help
 * @author ignum
*/

namespace Usluz\Core\Other\Help;
use \Usluz\Core\Error;



/**
 * Autoload trida pro automaticke nacitani a vytvareni trid
*/
class Autoload {
	/**
	 * Bajpasuje standardni 'new' - kontroluje, zdali vytvarena trida skutecne existuje...
	 * @param string $file
	 * @param string $error_name
	 * @return $class
	*/
	static public function _new($class) {
		if(class_exists($class)) {
			$count = func_num_args() - 1;
			$args  = func_get_args();
			array_shift($args);
			return self::create_from_varios_args($count, $args, $class);
		} else {
			throw new \Usluz\Core\Error\EClass\NotExists('File or Class "' . $class . '" does not exists!');
		}
	}

	/**
	 * @param int $number_of_args
	 * @param array $args
	 * @param object $class_name
	 * @return instance-of-$class_name
	*/
	static private function create_from_varios_args($number_of_args, array $args, $class_name) {
		$len  = count($args);
		$i    = 0;
		$_arg = '';
		for($i; $i < $len; $i++) {
			$_arg .= '$args[' . $i . ']';
			if($i < $len - 1) $_arg .= ', ';
		}

		$_r   = null;
		$_tmp = '$_r = new ' . $class_name . '(' . $_arg . ');';
		$_t   = eval($_tmp);

		if((!is_null($_t)) && !$_t)
			throw new Error\EClass\CouldNotCreate('Autoload::_new - eval critic error');
		if(!$_r)
			throw new Error\EClass\CouldNotCreate('Autoload::_new - could not create class ' . $class_name);

		return $_r;
	}
}
