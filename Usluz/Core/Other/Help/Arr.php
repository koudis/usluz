<?php
/**
 * @package Usluz\\Usluz\Core\Other\Help
 * @author ignum
*/

namespace Usluz\Core\Other\Help;
use \Usluz\Core\Error;



/**
 * Prace s polem, kterou nepodporuje def. PHP
*/
class Arr {
	/**
	 * @param int $number_of_args
	 * @param array $args
	 * @param object $class_name
	 * @return instance-of-$class_name
	*/
	static public function &array_merge_tree(array $to, array $from) {
		foreach($from as $_k => $_v) {
			if(isset($to[$_k])) {
				if(is_array($from[$_k])) {
					if(!is_array($to[$_k])) {
						$to[$_k] = $from[$_k];
					}
					if(is_array($to[$_k])) {
						$to[$_k] = &self::array_merge_tree($to[$_k], $from[$_k]);
					}
				}
				if(!is_array($from[$_k])) {
					if(!is_array($to[$_k])) {
						$to[$_k] = $from[$_k];
					}
					if(is_array($to[$_k])) {
						# nedelej nic ;)
					}
				}
			} else {
				$to[$_k] = $from[$_k];
			}
		}

		return $to;
	}
}
