<?php
/**
 * @package Usluz\\Usluz\Core\Other\Help
 * @author jan.kubalek
*/

namespace Usluz\Core\Other\Help;



/**
 * Trida/namespace pro prace se jmenymi prostory
*/
class NamespaceTools {

	/**
	 * Prevede namespace na path odpovidajici dane namespace
	 * @param  string $namespace
	 * @return string
	*/ 
	static public function namespace_to_path($namespace) {
		return preg_replace('%([a-zA-Z0-9])\\\\%', '\1/', $namespace) . '/';
	}
}
