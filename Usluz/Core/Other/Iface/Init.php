<?php
/**
 * @author jan.kubalek
*/

namespace Usluz\Core\Other\Iface;



/**
 * Inicializuje prostredi, registruje autoload procedury apod.
*/ 
interface Init {
	/**
	 * Nastavi prostredi
	 * @return boolean
	*/
	static public function init();
}
