<?php
/**
 *
 *
 *
*/

namespace Usluz\Core;
use \Usluz\Core\Error;



/**
 *
 *
*/
class GlobalDir {
	/**
	 * Array with structure
	 *   array(
	 *       dir => [
	 *           'type'   => dir_type
	 *           'access' => access    // as '0777'
	 *       ]
	 *   )
	 * @var array
	*/
	static public $dirs = [];

	/**
	 *
	*/
	static private $base_dir_exists = 0;

	/**
	 * Add dir to the stack or
	 * Create dir immediately if $force is true
	 * @param string
	 * @param octal-number
	 * @param boolean True - create dir immediately
	 * @return boolean
	*/
	static public function add($dir, $access = 0770, $force = 0) {
		if(isset(self::$dirs[$dir]))
			throw new Error\Data\DataConstraint("Dir with name '$dir' already exists");
		if($force) {
			$base_dir = GlobalConf::get("TEMPORARY_DIR");
			if(!static::$base_dir_exists) {
				if(!file_exists($base_dir))
					throw new \Exception("Please create directory '$base_dir'");
				static::$base_dir_exists = 1;
			}

			$path = $base_dir . '/' . $dir;
			if(file_exists($path))
				return true;
			$_tmp = mkdir($path, $access);
			if(!$_tmp)
				throw new \Exception("Cannot reate directory " . $path);
			return true;
		}

		self::$dirs[$dir] = [
				'access' => $access
			];
		return true;
	}

	/**
	 * Return all global registered dirs
	 * @return &$dirs
	*/
	static public function &get() {
		return self::$dirs;
	}

	/**
	 * Create all registered dirs (or check if exists)
	*/
	static public function create() {
		$base_dir = GlobalConf::get("TEMPORARY_DIR");
		if(!static::$base_dir_exists) {
			if(!file_exists($base_dir))
				throw new \Exception("Please create directory '$base_dir'");
			static::$base_dir_exists = 1;
		}

		foreach(self::$dirs as $dir => &$val) {
			if(file_exists($base_dir . '/' . $dir))
				continue;
			$_tmp = mkdir($base_dir . '/' . $dir, $val['access']);
			if(!$_tmp)
				throw new \Exception("Cannot reate directory " . $base_dir . '/' . $dir);
		}
	}
}
