<?php
/**
 * @author jan.kubalek
 * @package \Usluz\Core\GlobalConf
*/

namespace Usluz\Core\GlobalConf;
use \Usluz\Core\Other\Conf;



class Def implements Iface\InterfaceGlobalConf {
	static private $conf = array(
		'WDIR'                => USLUZ_ACT_DIR,
		'CONF_DIR'            => USLUZ_ACT_DIR . 'Usluz/conf/',
		'CONF_FILE_EXT'       => '.yaml',
		'MAIN_CONF_FILE_NAME' => 'main' . '.yaml',
		'TEMPORARY_DIR'       => '/tmp/usluz',
		'PROCESS_BASE_DIR'    => USLUZ_ACT_DIR . '/Usluz/',
		'MAX_CACHE'           => 86400, // one week
	);


	/**
	 * @see \Usluz\Core\GlobalConf\Iface\InterfaceGlobalConf
	 * @return array
	 */
	static public function &get_conf() {
		return static::$conf;
	}
}
