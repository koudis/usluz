<?php
/**
 * @author jan.kubalek
*/

namespace Usluz\Core\GlobalConf\Iface;



/**
 * Inicializuje prostredi, registruje autoload procedury apod.
*/ 
interface InterfaceGlobalConf {
	/**
	 * Vrati konfiguracni data prostredi
	 * @return boolean
	*/
	static public function &get_conf();
}
