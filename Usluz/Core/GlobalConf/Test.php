<?php
/**
 * @author jan.kubalek
 * @package \Usluz\Core\GlobalConf
*/

namespace Usluz\Core\GlobalConf;



class Test implements Iface\InterfaceGlobalConf {
	static private $conf = array(
		'CONF_DIR'         => USLUZ_ACT_DIR . '/Usluz/conf/',
		'PROCESS_BASE_DIR' => USLUZ_ACT_DIR . '/Usluz/',
	);


	/**
	 * @see \Usluz\Core\GlobalConf\Iface\InterfaceGlobalConf
	 * @return array
	 */
	static public function &get_conf() {
		$_t = array_replace(Def::get_conf(), static::$conf);
		return $_t;
	}
}
